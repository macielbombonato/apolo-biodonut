<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		
		<title>
			<s:message code="app.title" />
		</title>
		
		<link rel="icon" type="image/png" href='<c:url value="/resources/app/img/favicon.png" />'>
		
		<link rel="stylesheet" media="screen" href='<c:url value="/resources/plugin/bootstrap/css/bootstrap.css" />' />
		<link rel="stylesheet" href='<c:url value="/resources/plugin/bootstrap/css/bootstrap-responsive.css" />' />
		
		<link rel="stylesheet" href='<c:url value="/resources/app/css/app.css" />' />
		
	</head>
	
	<body>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script type="text/javascript" src='<c:url value="/resources/plugin/bootstrap/js/bootstrap.min.js" />'></script>
	
		<div class="container">
			<tiles:insertAttribute name="header" defaultValue="" />
			
			<div id="loadingDialog" class="modal hide fade span1" tabindex="-1" data-role="dialog" data-backdrop="static">
				<center>
					<img src='<c:url value="/resources/app/img/ajax-loader.gif" />' class="img-circle">
				</center>
			</div>
			
			<br /><br />
			
			<div class="row">
				<div class="span12">
					<c:choose>
						<c:when test="${error}">
							<div class="alert alert-error">
								${message}
							</div>		
						</c:when>
						<c:when test="${warn}">
							<div class="alert alert-warn">
								${message}
							</div>		
						</c:when>
						<c:when test="${msg}">
							<div class="alert alert-info">
								${message}
							</div>		
						</c:when>
					</c:choose>
				</div>
			</div>
			
			<div class="row">
				<div class="span12">
					<tiles:insertAttribute name="body" defaultValue="" />
				</div>
			</div>
			<div class="row">
				<div class="span12">
					<tiles:insertAttribute name="footer" defaultValue="" />
				</div>
			</div>
		</div>
		
		<script type="text/javascript">
			var App = function() {
				return {
					contextPath : '${pageContext.request.contextPath}',
					locale : '${sessionScope["org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE"]}',
					readOnly : ${readOnly != null ? readOnly : false }
				};
			}();
		</script>
		
		<!-- tag-it script -->
		<script src='<c:url value="/resources/plugin/tagit/js/tag-it.js" />'></script>
		
		<!-- cleditor script -->
		<script src='<c:url value="/resources/plugin/cleditor/jquery.cleditor.js" />'></script>
		
		<!-- application scripts -->
		<script type="text/javascript" src='<c:url value="/resources/app/js/app.js" />'></script>
		
		<!-- Scripts to load only if the user are in the form screen -->
		<security:authorize access="!isAuthenticated()">
			<script type="text/javascript" src='<c:url value="/resources/app/js/login.js" />'></script>
		</security:authorize>
		
		<script type="text/javascript" src='<c:url value="/resources/app/js/item-list-util.js" />'></script>
	</body>
</html>