<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<input type="hidden" id="id" name="id" value="${plasmid.id}" />

<div class="row">
	<div class="span6">
		<label for="dunetChart">
			<s:message code="plasmid.dunet.chart" />
		</label>
		<div class="thumbnail">
			<img id="dunetChart" src="data:image/png;base64,<c:out value='${plasmid.plasmidDunetChartAsString}'/>" />
		</div>
	</div>
	<c:if test="${newPlasmid != null && newPlasmid.plasmidDunetChartAsString != null}" >
		<div class="span6">
			<label for="newDunetChart">
				<s:message code="plasmid.new.dunet.chart" />
			</label>
			<div class="thumbnail">
				<img id="newDunetChart" src="data:image/png;base64,<c:out value='${newPlasmid.plasmidDunetChartAsString}'/>" />
			</div>
		</div>	
	</c:if>
</div>

<div class="row">
	<div class="span6">
		<label for="accession">
			<s:message code="common.accession" />
		</label>
		<input type="text" id="accession" name="accession" class="input-block-level" value="${plasmid.accession}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
	<div class="span6">
		<label for="version">
			<s:message code="plasmid.version" />
		</label>
		<input type="text" id="version" name="version" class="input-block-level" value="${plasmid.version}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
</div>

<div class="row">
	<div class="span12">
		<label for="locus">
			<s:message code="plasmid.locus" />
		</label>
		<input type="text" id="locus" name="locus" class="input-block-level" value="${plasmid.locus}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
</div>

<div class="row">
	<div class="span12">
		<label for="definition">
			<s:message code="plasmid.definition" />
		</label>
		<input type="text" id="definition" name="definition" class="input-block-level" value="${plasmid.definition}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
</div>

<div class="row">
	<div class="span12">
		<label for="keywords">
			<s:message code="plasmid.keywords" />
		</label>
		<input type="text" id="keywords" name="keywords" class="input-block-level" value="${plasmid.keywords}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
</div>

<div class="row">
	<div class="span12">
		<label for="source">
			<s:message code="plasmid.source" />
		</label>
		<input type="text" id="source" name="source" class="input-block-level" value="${plasmid.source}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
</div>

<div class="row">
	<div class="span12">
		<table class="table table-striped table-hover table-bordered">
			<caption>
				<strong>
					<s:message code="plasmid.references" />
				</strong>
			</caption>
			<thead>
				<tr>
					<th>
						<s:message code="plasmid.reference.origin" />
					</th>
					<th>
						<s:message code="plasmid.reference.authors" />
					</th>
					<th>
						<s:message code="plasmid.reference.title" />
					</th>
					<th>
						<s:message code="plasmid.reference.journal" />
					</th>
					<th>
						<s:message code="plasmid.reference.pubmed" />
					</th>
					<th>
						<s:message code="plasmid.reference.remark" />
					</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${plasmid.references}" var="reference" varStatus="status">
					<input type="hidden" name="references[${status.count - 1}].id" value="${reference.id}" />
					
					<tr>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${reference.origin}
								</c:when>
								<c:otherwise>
									<input type="text" name="references[${status.count - 1}].origin" value="${reference.origin}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${reference.authors}
								</c:when>
								<c:otherwise>
									<input type="text" name="references[${status.count - 1}].authors" value="${reference.authors}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${reference.title}
								</c:when>
								<c:otherwise>
									<input type="text" name="references[${status.count - 1}].title" value="${reference.title}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${reference.journal}
								</c:when>
								<c:otherwise>
									<input type="text" name="references[${status.count - 1}].journal" value="${reference.journal}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${reference.pubmed}
								</c:when>
								<c:otherwise>
									<input type="text" name="references[${status.count - 1}].pubmed" value="${reference.pubmed}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${reference.remark}
								</c:when>
								<c:otherwise>
									<input type="text" name="references[${status.count - 1}].remark" value="${reference.remark}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="span6">
		<table class="table table-striped table-hover table-bordered">
			<caption>
				<strong>
					<s:message code="plasmid.features" />
				</strong>
			</caption>
			<thead>
				<tr>
					<th>
						<s:message code="plasmid.feature.name" />
					</th>
					<th>
						<s:message code="plasmid.feature.startPosition" />
					</th>
					<th>
						<s:message code="plasmid.feature.endPosition" />
					</th>
					<th>
						<s:message code="plasmid.feature.direction" />
					</th>
					<th>
						<s:message code="plasmid.feature.details" />
					</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${plasmid.features}" var="feature" varStatus="status">
					<input type="hidden" name="features[${status.count - 1}].id" value="${feature.id}" />
					
					<tr>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${feature.name}
								</c:when>
								<c:otherwise>
									<input type="text" name="features[${status.count - 1}].name" value="${feature.name}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${feature.startPosition}
								</c:when>
								<c:otherwise>
									<input type="text" name="features[${status.count - 1}].startPosition" value="${feature.startPosition}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${feature.endPosition}
								</c:when>
								<c:otherwise>
									<input type="text" name="features[${status.count - 1}].endPosition" value="${feature.endPosition}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${feature.direction}
								</c:when>
								<c:otherwise>
									<input type="text" name="features[${status.count - 1}].direction" value="${feature.direction}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${readOnly}">
									${feature.details}
								</c:when>
								<c:otherwise>
									<input type="text" name="features[${status.count - 1}].details" value="${feature.details}" class="input-block-level" <c:if test="${readOnly}">readonly="true"</c:if> />
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
	<c:if test="${newPlasmid != null && newPlasmid.features != null}" >
		<div class="span6">
			<table class="table table-striped table-hover table-bordered">
				<caption>
					<strong>
						<s:message code="plasmid.new.features" />
					</strong>
				</caption>
				<thead>
					<tr>
						<th>
							<s:message code="plasmid.feature.name" />
						</th>
						<th>
							<s:message code="plasmid.feature.startPosition" />
						</th>
						<th>
							<s:message code="plasmid.feature.endPosition" />
						</th>
						<th>
							<s:message code="plasmid.feature.direction" />
						</th>
						<th>
							<s:message code="plasmid.feature.details" />
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${newPlasmid.features}" var="feature" varStatus="status">
						<input type="hidden" name="features[${status.count - 1}].id" value="${feature.id}" />
						
						<tr>
							<td>
								${feature.name}
							</td>
							<td>
								${feature.startPosition}
							</td>
							<td>
								${feature.endPosition}
							</td>
							<td>
								${feature.direction}
							</td>
							<td>
								${feature.details}
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</c:if>
</div>

<div class="row">
	<div class="span6">
		<table class="table table-hover table-condensed table-bordered">
			<caption>
				<strong>
					<s:message code="plasmid.sequence" />
				</strong>
			</caption>
			<thead>
				<tr>
					<th>
						<s:message code="plasmid.sequence.sequenceInitialPosition" />
					</th>
					<th>
						<s:message code="plasmid.sequence.value" />
					</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${plasmid.longSequences}" var="longSequence" varStatus="status">
					<tr>
						<td>
							<c:out value="${longSequence.sequenceInitialPosition}" escapeXml="false" />
						</td>
						<td>
							<code>
								<c:out value="${longSequence.value}" escapeXml="false" />
							</code>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<c:forEach items="${plasmid.sequences}" var="sequence" varStatus="status">
			<input type="hidden" name="sequences[${status.count - 1}].id" class="input-block-level" value="${sequence.id}" />
			<input type="hidden" name="sequences[${status.count - 1}].sequenceInitialPosition" class="input-block-level" value="${sequence.sequenceInitialPosition}" />
			<input type="hidden" name="sequences[${status.count - 1}].value" class="input-block-level" value="${sequence.value}" />
		</c:forEach>
	</div>
	
	<c:if test="${newPlasmid != null && newPlasmid.sequences != null}" >
		<div class="span6">
			<table class="table table-hover table-condensed table-bordered">
				<caption>
					<strong>
						<s:message code="plasmid.new.sequence" />
					</strong>
				</caption>
				<thead>
					<tr>
						<th>
							<s:message code="plasmid.sequence.sequenceInitialPosition" />
						</th>
						<th>
							<s:message code="plasmid.sequence.value" />
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${newPlasmid.longSequences}" var="longSequence" varStatus="status">
						<tr>
							<td>
								<c:out value="${longSequence.sequenceInitialPosition}" escapeXml="false" />
							</td>
							<td>
								<code>
									<c:out value="${longSequence.value}" escapeXml="false" />
								</code>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</c:if>
</div>