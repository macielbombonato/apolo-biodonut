<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<jsp:include page='_search-form.jsp'></jsp:include>

<fieldset>
	<legend>
		<s:message code="plasmid.list.title" />
	</legend>
	
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>
					#
				</th>
				<th>
					<s:message code="common.accession" />
				</th>
				<th>
					<s:message code="plasmid.locus" />
				</th>
				<th>
					<s:message code="plasmid.definition" />
				</th>
				<th>
					<s:message code="plasmid.version" />
				</th>
				<th>
					<s:message code="plasmid.keywords" />
				</th>
				<th>
					<s:message code="plasmid.source" />
				</th>
				<th>
					<s:message code="common.actions" />
				</th>
			</tr>
		</thead>
		
		<tbody>
			<c:forEach items="${plasmidList}" var="plasmid">
				<tr>
					<td>
						${plasmid.id}
					</td>
					<td>
						${plasmid.accession}
					</td>
					<td>
						${plasmid.locus}
					</td>
					<td>
						${plasmid.definition}
					</td>
					<td>
						${plasmid.version}
					</td>
					<td>
						${plasmid.keywords}
					</td>
					<td>
						${plasmid.source}
					</td>
					<td>
						<div class="btn-group">
							<a href='<s:url value="/plasmid/view"></s:url>/${plasmid.id}' class="btn" tabindex="-1">
								<s:message code="common.show" />
							</a>
							<button class="btn dropdown-toggle" data-toggle="dropdown" tabindex="-1">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href='<s:url value="/plasmid/edit"></s:url>/${plasmid.id}'>
										<s:message code="common.edit" />
									</a>
								</li>
								<li>
									<a href='<s:url value="/plasmid/remove"></s:url>/${plasmid.id}'>
										<s:message code="common.remove" />
									</a>
								</li>
							</ul>
						</div>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</fieldset>
