<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<fieldset>
	<legend>
		<s:message code="plasmid.view.title" />
	</legend>
	
	<div class="form-actions">
		<a href='<s:url value="/plasmid/input-data-assay"></s:url>/${plasmid.id}' class="btn btn-primary">
			<s:message code="plasmid.input.data.assay" />
		</a>

		<a href='<s:url value="/"></s:url>' class="btn">
			<s:message code="common.cancel" />
		</a>
	</div>
	
	<jsp:include page='_plasmid-form.jsp'></jsp:include>
	
</fieldset>
