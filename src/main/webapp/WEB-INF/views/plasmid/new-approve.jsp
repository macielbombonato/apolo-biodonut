<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<fieldset>
	<legend>
		<s:message code="plasmid.new.title" />
	</legend>

	<form id="plasmidForm" action="<s:url value="/plasmid/save"></s:url>" method="post">
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">
				<s:message code="common.save" /> 
			</button>

			<a href='<s:url value="/"></s:url>' class="btn">
				<s:message code="common.cancel" />
			</a>
		</div>
		
		<jsp:include page='_plasmid-form.jsp'></jsp:include>
	</form>
	
</fieldset>
