<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<fieldset>
	<legend>
		<s:message code="plasmid.input.data.assay" />
	</legend>

	<form id="plasmidForm" action="<s:url value="/plasmid/run-assay"></s:url>" method="post">
		
		<input type="hidden" id="plasmidId" name="plasmidId" value="${plasmid.id}"/>

		<div class="row">
			<div class="span12">
				<label for="inputName">
					<s:message code="plasmid.input.name" />
				</label>
				<input type="text" id="inputName" name="inputName" class="input-block-level" value="${inputedName}"/>
			</div>
		</div>
		
		<div class="row">
			<div class="span12">
				<label for="inputSequence">
					<s:message code="plasmid.input.sequence" />
				</label>
				<textarea rows="10" cols="80" id="inputSequence" name="inputSequence" class="input-block-level" >${inputedSequence}</textarea>
			</div>
		</div>
		
		<!-- Button to trigger modal -->
		<a href="#" id="electiveEnzymesButton" class="btn">
			<s:message code="enzyme.elective.title" />
		</a>
 
		<!-- Modal -->
		<div id="electiveEnzymesDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="electiveEnzymesLabel" aria-hidden="true" data-backdrop="static">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="electiveEnzymesLabel">
					<s:message code="enzyme.elective.title" />
				</h3>
			</div>
			<div class="modal-body">
				<div id="electiveEnzymesData" > </div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<s:message code="common.cancel" />
				</button>
			</div>
		</div>
		
		<div class="row">
			<c:if test="${initialEnzyme.id != null}" >
				<input type="hidden" readonly="readonly" id="enzymeInitialId" name="enzymeInitialId" class="input-block-level" value="${initialEnzyme.id}"/>
				
				<div class="span12">
					<table class="table table-striped table-hover table-bordered">
						<caption>
							<strong>
								<s:message code="enzyme.initial" />
							</strong>
						</caption>
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									<s:message code="common.name" />
								</th>
								<th>
									<s:message code="common.accession" />
								</th>
								<th>
									<s:message code="enzyme.codon.principal.value" />
								</th>
								<th>
									<s:message code="enzyme.codon.principal.position" />
								</th>
								<th>
									<s:message code="enzyme.codon.compl.value" />
								</th>
								<th>
									<s:message code="enzyme.codon.compl.position" />
								</th>
								<th>
									<s:message code="enzyme.sequence.court.initial" />
								</th>
								<th>
									<s:message code="plasmid.sequence.court.initial" />
								</th>
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td>
									${initialEnzyme.id}
								</td>
								<td>
									${initialEnzyme.name}
								</td>
								<td>
									${initialEnzyme.accession}
								</td>
								<td>
									${initialEnzyme.principalValue}
								</td>
								<td>
									${initialEnzyme.principalCourtPosition}
								</td>
								<td>
									${initialEnzyme.complementValue}
								</td>
								<td>
									${initialEnzyme.complementCourtPosition}
								</td>
								<td>
									<input type="text" 
											id="enzymeSequenceInitialCourtPosition" 
											name="enzymeSequenceInitialCourtPosition" 
											readonly="readonly" 
											class="span1"
											value="${enzymeSequenceInitialCourtPosition}"
										/>
								</td>
								<td>
									<select id="enzymePlasmidInitialCourtPosition" name="enzymePlasmidInitialCourtPosition" size="1" class="input-block-level">
										<c:forEach items="${enzymePlasmidInitialCourtPositionList}" var="courtPosition">
											<option value="${courtPosition}">
												${courtPosition}
											</option>
										</c:forEach>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</c:if>
			<c:if test="${finalEnzyme.id != null}" >
				<input type="hidden" readonly="readonly" id="enzymeFinalId" name="enzymeFinalId" class="input-block-level" value="${finalEnzyme.id}"/>
				
				<div class="span12">
					<table class="table table-striped table-hover table-bordered">
						<caption>
							<strong>
								<s:message code="enzyme.final" />
							</strong>
						</caption>
						<thead>
							<tr>
								<th>
									#
								</th>
								<th>
									<s:message code="common.name" />
								</th>
								<th>
									<s:message code="common.accession" />
								</th>
								<th>
									<s:message code="enzyme.codon.principal.value" />
								</th>
								<th>
									<s:message code="enzyme.codon.principal.position" />
								</th>
								<th>
									<s:message code="enzyme.codon.compl.value" />
								</th>
								<th>
									<s:message code="enzyme.codon.compl.position" />
								</th>
								<th>
									<s:message code="enzyme.sequence.court.final" />
								</th>
								<th>
									<s:message code="plasmid.sequence.court.final" />
								</th>
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td>
									${finalEnzyme.id}
								</td>
								<td>
									${finalEnzyme.name}
								</td>
								<td>
									${finalEnzyme.accession}
								</td>
								<td>
									${finalEnzyme.principalValue}
								</td>
								<td>
									${finalEnzyme.principalCourtPosition}
								</td>
								<td>
									${finalEnzyme.complementValue}
								</td>
								<td>
									${finalEnzyme.complementCourtPosition}
								</td>
								<td>
									<input type="text" 
											id="enzymeSequenceFinalCourtPosition" 
											name="enzymeSequenceFinalCourtPosition" 
											readonly="readonly" 
											class="span1"
											value="${enzymeSequenceFinalCourtPosition}"
										/>
								</td>
								<td>
									<select id="enzymePlasmidFinalCourtPosition" name="enzymePlasmidFinalCourtPosition" size="1" class="input-block-level">
										<c:forEach items="${enzymePlasmidFinalCourtPositionList}" var="courtPosition">
											<option value="${courtPosition}">
												${courtPosition}
											</option>
										</c:forEach>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</div>			
			</c:if>
		</div>
		
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">
				<s:message code="common.ok" /> 
			</button>

			<a href='<s:url value="/"></s:url>' class="btn">
				<s:message code="common.cancel" />
			</a>
		</div>
	</form>
</fieldset>


<script type="text/javascript">
	$(document).ready(function() {
		$('#electiveEnzymesButton').click(function(e) {
			$('#plasmidForm').attr('action', '<s:url value="/plasmid/show-elective-enzymes"></s:url>');
			$('#plasmidForm').submit();
		});
	});
</script>