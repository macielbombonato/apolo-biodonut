<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div class="row">
	<div class="span12">
		<div class="well">
			<fieldset>
				<legend>
					Portal
				</legend>
				<p align="justify">
					O objetivo do sistema é oferecer ao usuário uma interface simples e que possibilite que a simulação do ensaio seja feita com poucos cliques.
					Também temos como foco criar a possibilidade de que os dados de antes e depois do ensaio sejam facilmente comparados.
				</p>
				<p align="justify">
					Os administradores e usuários autenticados podem cadastrar plasmídeos e enzimas de corte que ficam disponíveis para acesso (consulta) público
					de forma que os usuários visitantes possam consultar toda a base de dados e possam utilizar estes dados para simular ensaios de clonagem.
				</p>
				<div class="well">
					<fieldset>
						<legend>
							Simulação de ensaio
						</legend>
						
						<p align="justify">
							Primeiramente o usuário deve informar o nome da sequência que ele deseja inserir no plasmídeo para que esta seja facilmente identificada
							no plasmídeo e na listagem de características do plasmídeo após a conclusão do ensaio.
						</p>
						<p align="justify">
							Em seguida, o usuário insere a sequência que deseja incluir no plasmídeo e o sistema calcula quais são as enzimas de corte que são compatíveis
							com o plasmídeo escolhido e com a sequência informada.
						</p>
						<p align="justify">
							Para fazer essa análise, buscamos todas as enzimas de corte que fazem parte da base de dados do Biodunet e cada uma é validada da seguinte forma:
						</p>
						<p align="justify">
							Primeiramente o sistema valida se o codon complementar foi informado na enzima de corte e em caso positivo, o sistema tenta encaixá-lo
							no codon principal, abaixo temos dois exemplos, o primeiro fazendo o encaixe à direita e o segundo fazendo o encaixe à esquerda.
						</p>
						
						<div class="well">
							<fieldset>
								<legend>
									Encaixe à direita
								</legend>
								
								<table>
									<TBODY class="ui-datatable-data ui-widget-content">
										<tr>
											<td>Principal</td>
											<td>G</td>
											<td>C</td>
											<td>C</td>
											<td>G</td>
											<td>C</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td>|</td>
											<td>|</td>
											<td>|</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td>Complementar</td>
											<td></td>
											<td></td>
											<td>G</td>
											<td>C</td>
											<td>G</td>
											<td>G</td>
											<td>C</td>
										</tr>
									</TBODY>
								</table>
								<br />
								<p align="justify">
									Considerando que na extremidade direita do codon complementar existem dois nucleotídeos sem o par acima, pode-se considerar que estes 
									nucleotídeos podem ser complementados com seus respectivos pares no codon principal e com isso, é gerada a seguinte sequência:
								</p>
								<table>
									<TBODY class="ui-datatable-data ui-widget-content">
										<tr>
											<td>G</td>
											<td>C</td>
											<td>C</td>
											<td>G</td>
											<td>C</td>
											<td><b>C</b></td>
											<td><b>G</b></td>
										</tr>
									</TBODY>
								</table>
							</fieldset>
						</div>
						
						<div class="well">
							<fieldset>
								<legend>
									Encaixe à esquerda
								</legend>
								
								<table>
									<TBODY class="ui-datatable-data ui-widget-content">
										<tr>
											<td>Principal</td>
											<td></td>
											<td></td>
											<td>C</td>
											<td>C</td>
											<td>G</td>
											<td>C</td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td>|</td>
											<td>|</td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td>Complementar</td>
											<td>G</td>
											<td>C</td>
											<td>G</td>
											<td>G</td>
											<td></td>
											<td></td>
										</tr>
									</TBODY>
								</table>
								<br />
								<p align="justify">
									Considerando que na extremidade esquerda do codon complementar existem dois nucleotídeos sem o par acima, pode-se considerar que estes 
									nucleotídeos podem ser complementados com seus respectivos pares no codon principal e com isso, é gerada a seguinte sequência:
								</p>
								<table>
									<TBODY class="ui-datatable-data ui-widget-content">
										<tr>
											<td><b>C</b></td>
											<td><b>G</b></td>
											<td>C</td>
											<td>C</td>
											<td>G</td>
											<td>C</td>
										</tr>
									</TBODY>
								</table>
							</fieldset>
						</div>
						
						<p align="justify">
							A forma que o sistema utiliza para inferir a posição do encaixe é validando quantos caracteres podem ser encaixados de cada lado e elegendo o lado
							que encaixar mais caracteres (caso seja possível encaixar dos dois lados), por exemplo, na primeira situação informada acima, se tentarmos encaixar 
							o codon complementar no codon principal à esquerda, será possível fazê-lo com apenas um caracter, já pela direita são três caracteres, logo, o 
							sistema opta por fazer o encaixe pela direita.
						</p>
						<p align="justify">
							No segundo exemplo passado, podemos fazer o encaixe à direita com apenas um caracter também e pela esquerda são dois, e com isso, o sistema encaixa
							os codons pela esquerda.
						</p>
						
						<p align="justify">
							É possível cadastrar enzimas de corte sem informar o codon complementar ou informá-lo de forma que o encaixe não é necessário, 
							conforme exemplo abaixo:
						</p>
						
						<div class="well">
							<fieldset>
								<legend>
									Não há necessidade de encaixe
								</legend>
								
								<p align="justify">
									No caso da primeira situação, podemos considerar que ao cadastrar a enzima, o codon complementar não foi informado ou o encaixe dos dois codons
									não possui sobras, ou seja, o encaixe é perfeito, conforme exemplo abaixo:
								</p>
								<table>
									<TBODY class="ui-datatable-data ui-widget-content">
										<tr>
											<td>Principal</td>
											<td>G</td>
											<td>G</td>
										</tr>
										<tr>
											<td></td>
											<td>|</td>
											<td>|</td>
										</tr>
										<tr>
											<td>Complementar</td>
											<td>C</td>
											<td>C</td>
										</tr>
									</TBODY>
								</table>
								<br />
								<p align="justify">
									Neste caso, o sistema considera apenas o codon principal da enzima de corte para realizar as validações seguintes e com isso, o sistema deverá buscar no plasmídeo e na sequência
									o seguinte texto:
								</p>
								<table>
									<TBODY class="ui-datatable-data ui-widget-content">
										<tr>
											<td>G</td>
											<td>G</td>
										</tr>
									</TBODY>
								</table>
							</fieldset>
						</div>
		
						<p align="justify">
							Quando sequência gerada é encontrada no plasmídeo, temos a indicação de que esta enzima de corte é compatível com o plasmídeo. Em seguida, 
							a mesma validação é feita com a sequência informada pelo usuário, e tendo a dupla positiva, temos a indicação de que esta enzima poderá ser
							utilizada no ensaio.
						</p>
						<p align="justify">
							O passo seguinte, é validar todas as posições que a sequência pode ser utilizada em ambos (plasmídeo e sequência) e com isso, possibilitar ao 
							usuário que a posição de corte seja escolhida no plasmídeo. 
							
						</p>
						<p align="justify">
							Na sequência informada pelo usuário, o sistema sempre indicará para a enzima de corte inicial a primeira posição de corte que foi encontrada 
							e para a enzima de corte final a última posição, assim, o sistema tenta aproveitar ao máximo a área da sequência informada pelo usuário para 
							inserí-la no plasmídeo.
						</p>
						<p align="justify">
							Finalizando o preenchimento dos dados, o usuário informa qual a direção da sequência inserida por ele:
						</p>
						<table class="table table-striped table-hover table-bordered">
							<THEAD>
								<tr>
									<th class="ui-state-default">
										Codon
									</th>
									<th class="ui-state-default">
										Símbolo
									</th>
									<th class="ui-state-default">
										Descrição
									</th>
								</tr>
							</THEAD>
							<TBODY class="ui-datatable-data ui-widget-content">
								<tr>
									<td>
										<b>Principal</b>
									</td>
									<td align="center">
										<b>+</b>
									</td>
									<td>
										Seta em sentido horário
									</td>
								</tr>
								<tr>
									<td>
										<b>Complementar</b>
									</td>
									<td align="center">
										<b>-</b>
									</td>
									<td>
										Seta em sentido anti-horário
									</td>
								</tr>
								<tr>
									<td>
										<b>Neutra</b>
									</td>
									<td align="center">
										<b>.</b>
									</td>
									<td>
										Sem seta
									</td>
								</tr>
							</TBODY>
						</table>
						
						<p align="justify">
							E clica no botão <b>Executar ensaio</b>.
						</p>
						
						<p align="justify">
							Neste momento o sistema processa as informações coletadas durante o processo de preenchimento e às valida de forma que:
						</p>
						
						<ul>
							<li>
								As posições de corte inicial não sejam maiores que as posições de corte final;
							</li>
							<li>
								A sequência seja remontada, retirando a parte do plasmídeo da área cortada na seleção e incluíndo em seu lugar a sequência
								informada pelo usuário (neste passo, o sistema inclui <i>tags HTML</i> para que seja possível destacar em negrito a sequência
								informada pelo usuário);
							</li>
							<li>
								As características do plasmídeo sejam remapeadas (as características que estiverem na área do corte serão retiradas do plasmídeo 
								e as que estiverem após a posição de corte final serão remapeadas considerando o novo tamanho do plasmídeo, ou seja, a posição
								atual da característica é subtraída da diferença de tamanho que o plasmídeo acabou tendo após o ensaio, onde devemos considerar que, 
								esta diferença poderá ser negativa caso o plasmídeo tenha aumentado de tamanho e com isso, a nova posição sofrerá uma soma, 
								abaixo segue exemplos);
							</li>
						</ul>
						
						<table class="table table-striped table-hover table-bordered">
							<THEAD>
								<tr>
									<th width="15%" class="ui-state-default">
										Posição Antes do Ensaio
									</th>
									<th width="15%" class="ui-state-default">
										Tamanho Antes do Ensaio
									</th>
									<th width="15%" class="ui-state-default">
										Tamanho Após o Ensaio
									</th>
									<th width="40%" class="ui-state-default">
										Conta
									</th>
									<th width="15%" class="ui-state-default">
										Resultado
									</th>
								</tr>
							</THEAD>
		
							<TBODY class="ui-datatable-data ui-widget-content">
								<tr>
									<td align="center">
										10.000
									</td>
									<td align="center">
										15.000
									</td>
									<td align="center">
										18.000
									</td>
									<td align="center">
										10.000 - (15.000 - 18.000)
									</td>
									<td align="center">
										13.000
									</td>
								</tr>
								<tr>
									<td align="center">
										10.000
									</td>
									<td align="center">
										15.000
									</td>
									<td align="center">
										10.000
									</td>
									<td align="center">
										10.000 - (15.000 - 10.000)
									</td>
									<td align="center">
										5.000
									</td>
								</tr>
							</TBODY>
						</table>
						
						<p align="justify">
							Após a conclusão destes cálculos e ajustes, será exibida novamente a tela de visualização do plasmídeo, agora, com dois gráficos, 
							duas tabelas de características e duas tabelas de sequência, lado-a-lado, para que o usuário possa comparar ambos os casos com mais 
							facilidade.
						</p>
						
						<p align="justify">
							No gráfico, a sequência informada pelo usuário ficará destacada sempre na cor vermelha, no centro será exibido o nome do plasmídeo
							mais o nome da sequência informada e as enzimas de corte utilizadas também serão identificadas.
						</p>
					</fieldset>
				</div>
				
				<div class="well">
					<fieldset>
						<legend>
							Carga de plasmídeos e enzimas de corte
						</legend>
						
						<p align="justify">
							Acessando o site <a href="http://ncbi.nlm.nih.gov" target="_blank">http://ncbi.nlm.nih.gov</a>, é possível obter arquivos para realizar a carga de plasmídeos
							e enzimas de corte, onde estes arquivos no formato texto são interpretados pelo sistema e convertidos de forma que seus dados sejam separados em um modelo relacional conforme segue:
						</p>
						
						<div class="well">
							<fieldset>
								<legend>
									Modelo relacional
								</legend>
								
								<div class="thumbnail">
									<img id="dunetChart" src='<c:url value="/resources/app/img/MER.png" />' />
								</div>
								
								<p align="center">
									Imagem extraída com o programa MySQL Workbrench 5.2.39 CE da Oracle
								</p>
							</fieldset>
						</div>
						
						<p align="justify">
							Neste modelo relacional, o sistema separa os dados encontrados no arquivo em suas respectivas entidades, e no caso específico da sequência do plasmídeo, o 
							sistema quebra o texto em pequenas subssequências de 10 caracteres armazenando-as em uma tabela separada (sequence) a fim de possibilitar o uso de qualquer banco de dados
							relacional, por exemplo, MySQL, SQL Server, Oracle ou PostgreeSQL e tentar algum ganho de performance na execução de consultas.
						</p>
						
						<p align="justify">
							Abaixo segue uma lista de exemplo de arquivos de plasmídeos que podem ser importados pelo sistema:
						</p>
						
						<table class="table table-striped table-hover table-bordered">
							<THEAD>
								<tr>
									<th width="40%" class="ui-state-default">
										CD Acesso
									</th>
									<th width="60%" class="ui-state-default">
										Arquivo
									</th>
								</tr>
							</THEAD>
		
							<TBODY class="ui-datatable-data ui-widget-content">
								<tr>
									<td align="left">
										M77811 M77798
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/Bluescribe.txt" />' target="_blank">Bluescribe.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										X52325
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pBluescript.txt" />' target="_blank">pBluescript.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										AF234315
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pCAMBIA2300_sequence.gb" />' target="_blank">pCAMBIA2300_sequence.gb</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										L20317
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pET.txt" />' target="_blank">pET.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										X65304
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pGEM-3Z.txt" />' target="_blank">pGEM-3Z.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										X65309
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pGEM-5Z.txt" />' target="_blank">pGEM-5Z.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										AJ007829
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pGreen.txt" />' target="_blank">pGreen.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										EF590266
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pGreenII.txt" />' target="_blank">pGreenII.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										AJ311872
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pHannibal.txt" />' target="_blank">pHannibal.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										AF489904
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pHellsgate.txt" />' target="_blank">pHellsgate.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										AJ311873
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pKannibal.txt" />' target="_blank">pKannibal.txt</a>
									</td>
								</tr>
								<tr>
									<td align="left">
										U12391
									</td>
									<td align="left">
										<a href='<c:url value="/resources/app/files/pSport2.txt" />' target="_blank">pSport2.txt</a>
									</td>
								</tr>
							</TBODY>
						</table>
						
						<p align="justify">
							Quanto às enzimas de corte, existem duas formas de cadastro:
						</p>
						
						<ul>
							<li>
								Inclusão informando campo a campo;
							</li>
							<li>
								Carga de lote de enzimas de corte via importação de arquivo;
							</li>
						</ul>
						
						<p align="justify">
							O padrão de importação é muito parecido com o que é feito no caso de plasmídeos, diferenciando que nas enzimas de corte, é possível importar um lote de 
							enzimas em um mesmo arquivo, abaixo segue um exemplo de arquivo que possui quase 400 enzimas que são importadas em apenas uma operação.
						</p>
						
						<table class="table table-striped table-hover table-bordered">
							<THEAD>
								<tr>
									<th width="100%" class="ui-state-default">
										Arquivo
									</th>
								</tr>
							</THEAD>
		
							<TBODY class="ui-datatable-data ui-widget-content">
								<tr>
									<td align="left">
										<a href='<c:url value="/resources/app/files/Bairoch_file_Restriction_enzymes.txt" />' target="_blank">Bairoch_file_Restriction_enzymes.txt</a>
									</td>
								</tr>
							</TBODY>
						</table>
						
					</fieldset>
				</div>
				
				<div class="well">
					<fieldset>
						<legend>
							Tecnologias utilizadas
						</legend>
						
						<ul>
							<li>
								Java 1.5+, Java EE 6
							</li>
							<li>
								Apache Tomcat 7.0.27+
							</li>
							<li>
								Apache Maven 2.2
							</li>
							<li>
								Hibernate / JPA
							</li>
							<li>
								Spring 3.1.3
							</li>
							<li>
								Spring MVC 3.1.3
							</li>
							<li>
								Spring Security 3.1.3
							</li>
							<li>
								Twitter Bootstrap
							</li>
							<li>
								MySQL 5.5
							</li>
							<li>
								Graphics 2D
							</li>
						</ul>
					</fieldset>
				</div>
			</fieldset>
		</div>
	</div>
</div>
