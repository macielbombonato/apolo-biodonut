<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div class="row">
	<div class="span12">
		<div class="well">
			<fieldset>
				<legend>
					Eloá Carolina
				</legend>
				
				<p align="justify">
					Preencher...
				</p>
				<p align="justify">
					No <b>Biodunet</b>, responsável pela pesquisa e embasamento teórico que define todo o sistema. 
				</p>
				
				<p align="justify">
					<b>Links:</b>
				</p>					
				<ul>
					<li>
						<a href="https://profiles.google.com/109897695470527336981" target="_blank">Google Profiles</a>
					</li>
				</ul>					
			</fieldset>
		</div>
		
		<div class="well">
			<fieldset>
				<legend>
					Maciel Escudero Bombonato
				</legend>
				
				<p align="justify">
					Graduado em 2005 em Ciência da Computação pela instituição Centro Universitário de Araraquara - UNIARA.
				</p>
				<p align="justify">
					Na área de TI desde 1999 dando aulas para alunos iniciantes e atuando como analista de suporte. Em 2005 início de atividades como desenvolvedor
					tendo maior foco nas plataformas Java (Desktop e Web) e Oracle (PL/SQL, Forms 6i e Reports 6i).
				</p>
				
				<p align="justify">
					No <b>Biodunet</b>, responsável pelo desenho arquitetônico e desenvolvimento da aplicação. 
				</p>
	
				<p align="justify">
					<b>Links:</b>
				</p>					
				<ul>
					<li>
						<a href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4356281Y6" target="_blank">Lattes</a>
					</li>
					<li>
						<a href="http://br.linkedin.com/in/macielbombonato" target="_blank">Linkedin</a>
					</li>
					<li>
						<a href="https://masterbranch.com/maciel.bombonato" target="_blank">Masterbranch</a>
					</li>
					<li>
						<a href="http://macielbombonato.blogspot.com.br/" target="_blank">Blog</a>
					</li>
				</ul>
			</fieldset>
		</div>
		
		<div class="well">
			<fieldset>
				<legend>
					Marco Aurélio Takita
				</legend>
				
				<p align="justify">
					Bacharel em Ciências Biológicas pela Universidade de São Paulo (1988), Licenciado em Ciências Biológicas pela Universidade de São Paulo (1988) e Doutor em Ciências Biológicas (Bioquímica) pela Universidade de São Paulo (1996). Atualmente é pesquisador científico V do Instituto Agronômico de Campinas. Tem experiência na área de Biologia Celular e Molecular, com ênfase em Biologia Molecular de Plantas, atuando principalmente nos seguintes temas: genomas, genômica funcional de plantas e bactérias fitopatogênicas, fitopatogenicidade, e bioinformática.
				</p>
				<p align="justify">
					Professor orientador do time. 
				</p>
	
				<p align="justify">
					<b>Links:</b>
				</p>					
				<ul>
					<li>
						<a href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4793171P5" target="_blank">Lattes</a>
					</li>
				</ul>				
			</fieldset>
		</div>
		
		<div class="well">
			<fieldset>
				<legend>
					William Paiva
				</legend>
				
				<p align="justify">
					Atualmente no Mestrado em Tecnologia e Inovação na Faculdade de Tecnologia da Universidade Estadual de Campinas e graduado em 2011 como Tecnólogo em Informática pela mesma instituição.
				</p>
				<p align="justify">
					Atuando na área de informática desde 2001, iniciando com atendimento ao usuário e ensino de informática. Experiência como desenvolvedor web entre 2007 e 2009, nas linguagens PHP e .NET. Grande interesse na área de computação gráfica e Teoria dos Grafos e Redes Complexas.
				</p>
				<p align="justify">
					No <b>Biodunet</b>,  responsável por parte da elaboração e desenvolvimento do serviço de desenho do gráfico do plasmídeo.   
				</p>
	
				<p align="justify">
					<b>Links:</b>
				</p>					
				<ul>
					<li>
						<a href="http://lattes.cnpq.br/5616956169592101" target="_blank">Lattes</a>
					</li>
					<li>
						<a href="http://www.quenerd.com.br/" target="_blank">Blog</a>
					</li>
				</ul>
			</fieldset>
		</div>
	</div>
</div>