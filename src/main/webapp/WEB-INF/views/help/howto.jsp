<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div class="row">
	<div class="span12">
		<div class="well">
			<fieldset>
				<legend>
					Simulação de ensaio de clonagem
				</legend>
				
				<p align="justify">
					O sistema <b>Biodunet</b> possibilita a simulação de um ensaio de clonagem, onde para tal, é necessário seguir os seguintes passos:
				</p>
				<ul>
					<li>
						Acessar o menu <b>Plasmídeo</b> e em seguida, <b>Listar</b> ou em <b>Buscar</b>;
					</li>
					<li>
						Escolher um dos plasmideos que aparecem na lista clicando no botão <b>Ver</b>;
					</li>
					<li>
						Caso tenha optado pela busca, informe parte do conteúdo de um dos campos informados e clique em <b>Buscar</b> e em seguida realize o passo acima;
					</li>
				</ul>
				<p align="justify">
					O plasmideo selecionado será exibido na tela de forma detalhada. Para dar andamento no processo de simulação de ensaio, 
					clique no botão <b> Incluir dados para simulação de ensaio</b>. Isso irá redirecioná-lo para a tela onde algumas informações devem ser
					providas para que o sistema possa dar andamento no processo.
				</p>
				<p align="justify">
					Neste passo, será necessário informar ao sistema:
				</p>
				<ul>
					<li>
						<b>Sequência a ser inserida no plasmídeo</b>: Sequência que será inserida no plasmideo (após inserir a sequência neste campo, o usuário
						deve licar no botão <b>Incluir dados para simulação de ensaio</b> para que a lista de s de corte elegíveis seja processada);
					</li>
					<li>
						Se existirem s que possam ser utilizadas para cortar o plasmideo e a sequência informada, a tabela será preenchida e para
						selecionar uma , o usuário deve clicar no botão <b>Selecione como inicial</b> para selecionar uma  para fazer o corte
						inicial e no botão <b>Selecione como final</b> para selecionar uma  para fazer o corte final;
					</li>
					<li>
						Quando selecionada, a  aparecerá em um painel abaixo da tabela, neste momento o usuário deverá informar a posição de corte
						inicial e final no plasmideo e na sequência, selecionando um dos valores que aparecem nos combos <b>Corte no Plasmídeo</b> e
						<b>Corte na Sequência</b> para cada uma das s selecionadas;
					</li>
					<li>
						O último passo de preenchimento consiste em selecionar a direção que deverá aparecer no gráfico selecionando uma das opções que aparecem 
						no final do formulário e em seguida clicar no botão <b>simular ensaio</b>;
					</li>
				</ul>
			</fieldset>
		</div>
		
		<div class="well">
			<fieldset>
				<legend>
					Apenas Usuário autenticado
				</legend>
				<div class="well">
					<fieldset>
						<legend>
							Inclusão e Remoção de Plasmideos
						</legend>
						
						<p align="justify">
							O sistema dá suporte a inclusão de plasmideos através da importação de arquivos que podem ser extraídos do 
							site do NCBI (<a href="http://ncbi.nlm.nih.gov" target="_blank">http://ncbi.nlm.nih.gov</a>).
						</p>
						<p align="justify">
							O processo de importação deve ser feito através do menu <b>Plasmídeo</b> e em seguida <b>Carregar Arquivo</b> e se 
							divide em 3 passos conforme segue:
						</p>
						<ul>
							<li>
								Escolher o arquivo em seu computador e enviá-lo;
							</li>
							<li>
								Conferir os dados carregados e caso necessário, retirar os mapeamentos que são considerados "como irrelevantes";
							</li>
							<li>
								Clicar em <b>Salvar</b> para que o processo seja concluído;
							</li>
						</ul>
						<p align="justify">
							Após a conclusão deste processo é possível remover o plasmideo, onde, para isso, é necessário acessar o menu <b>Listar</b>,
							escolher o plasmideo na lista, podendo ou não utilizar os filtros disponíveis na tela, e em seguida clicar no botão <b>Remover</b> 
							que está na linha do plasmideo desejado.
						</p>
						<p align="justify">
							O plasmideo selecionado irá aparecer na tela de forma detalhada. Para concluir a operação clique no botão <b>Remover</b>.
						</p>
					</fieldset>
				</div>
				
				<div class="well">
					<fieldset>
						<legend>
							Inclusão e Remoção de Enzimas de corte
						</legend>
						
						<p align="justify">
							O sistema dá suporte a inclusão de s de corte em lote através da importação de arquivos que podem ser extraídos do 
							site do NCBI (<a href="http://ncbi.nlm.nih.gov" target="_blank">http://ncbi.nlm.nih.gov</a>).
						</p>
						<p align="justify">
							O processo de importação deve ser feito através do menu <b>Enzima</b> e em seguida <b>Carregar Arquivo</b> e se 
							divide em 2 passos conforme segue:
						</p>
						<ul>
							<li>
								Escolher o arquivo em seu computador e enviá-lo;
							</li>
							<li>
								Será apresentada a lista de s que foram extraídas do arquivo para conferência do usuário;
							</li>
							<li>
								Clicar em <b>Salvar</b> para que o processo seja concluído;
							</li>
						</ul>
						<p align="justify">
							Após a conclusão deste processo é possível remover s e/ou editá-las, onde, para isso, é necessário acessar o menu <b>Listar</b>,
							escolher a  na lista, podendo ou não utilizar os filtros disponíveis na tela, e em seguida clicar em um dos botões de ação 
							que está na linha da  de corte desejada.
						</p>
						<p align="justify">
							No caso de remoção a  irá aparecer na tela de forma detalhada para que o usuário confirme a ação clicando no botão <b>Remover</b>.
						</p>
						<p align="justify">  
							No processo de edição o fluxo é o mesmo da remoção, porém, os campos são editáveis e a ação é concluída ao clicar no botão <b>Salvar</b>.
						</p>
					</fieldset>
				</div>
			</fieldset>
		</div>
	</div>
</div>

