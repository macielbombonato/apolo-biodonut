<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<ul class="nav">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<s:message code="plasmid" /> <b class="caret"></b>
		</a>
		
		<ul class="dropdown-menu">
			<security:authorize  ifAnyGranted="ROLE_ADMIN, ROLE_DATA_MANAGER">
				<li>
					<a href='<s:url value="/plasmid/new"></s:url>'>
						<s:message code="plasmid.new.upload" />
					</a>
				</li>
				
				<li class="divider" />
			</security:authorize>
			
			<li>
				<a href='<s:url value="/plasmid/search-form"></s:url>'>
					<s:message code="common.search" />
				</a>
			</li>
			<li>
				<a href='<s:url value="/plasmid/list"></s:url>'>
					<s:message code="common.list" />
				</a>
			</li>
		</ul>
	</li>		
</ul>