<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<fieldset>
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>
					<s:message code="common.name" />
				</th>
				<th>
					<s:message code="common.accession" />
				</th>
				<th>
					<s:message code="enzyme.initial" />
				</th>
				<th>
					<s:message code="enzyme.final" />
				</th>
			</tr>
		</thead>
		
		<tbody>
			<c:forEach items="${enzymeList}" var="enzyme">
				<input type="hidden" id="enzymeId-${enzyme.id}" value="${enzyme.id}"/>
				<input type="hidden" id="enzymeName-${enzyme.id}" value="${enzyme.name}"/>
				<input type="hidden" id="enzymeAccession-${enzyme.id}" value="${enzyme.accession}"/>
				<input type="hidden" id="enzymePrincipalValue-${enzyme.id}" value="${enzyme.principalValue}"/>
				<input type="hidden" id="enzymePrincipalCourtPosition-${enzyme.id}" value="${enzyme.principalCourtPosition}"/>
				<input type="hidden" id="enzymeComplementValue-${enzyme.id}" value="${enzyme.complementValue}"/>
				<input type="hidden" id="enzymeComplementCourtPosition-${enzyme.id}" value="${enzyme.complementCourtPosition}"/>
				
				<tr>
					<td>
						${enzyme.name}
					</td>
					<td>
						${enzyme.accession}
					</td>
					<td>
						<a href='#' class="btn" onclick="chooseAsInitialEnzyme(${enzyme.id});">
							<i class="icon-check" > </i>
						</a>
					</td>
					<td>
						<a href='#' class="btn" onclick="chooseAsFinalEnzyme(${enzyme.id});">
							<i class="icon-check" > </i>
						</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</fieldset>
