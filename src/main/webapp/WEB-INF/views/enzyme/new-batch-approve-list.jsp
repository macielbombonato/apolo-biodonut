<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src='<c:url value="/resources/app/js/enzyme.js" />'></script>

<fieldset>
	<legend>
		<s:message code="enzyme.new.batch.title" />
	</legend>
	
	<div class="row">
		<div class="span12">
			<form class="form-inline enzymeForm" action="<s:url value="/enzyme/save-batch"></s:url>" method="post">
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">
						<s:message code="common.save.all" /> 
					</button>
		
					<a href='<s:url value="/"></s:url>' class="btn">
						<s:message code="common.cancel" />
					</a>
				</div>
			
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th class="span1">
								#
							</th>
							<th class="span2">
								<s:message code="common.name" />
							</th>
							<th class="span2">
								<s:message code="common.accession" />
							</th>
							<th class="span2">
								<s:message code="enzyme.codon.principal.value" />
							</th>
							<th class="span1">
								<s:message code="enzyme.codon.principal.position" />
							</th>
							<th class="span2">
								<s:message code="enzyme.codon.compl.value" />
							</th>
							<th class="span1">
								<s:message code="enzyme.codon.compl.position" />
							</th>
							<th class="span1">
							</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${enzymeList}" var="enzyme" varStatus="status">
							<tr id="enzyme_${enzyme.accession}">
								<td class="span1">
									<span>${status.count} </span>
								</td>
								<td class="span2">
									<input type="text" name="enzymes[${status.count - 1}].name" class="input-block-level" value="${enzyme.name}" />
								</td>
								<td class="span2">
									<input type="text" name="enzymes[${status.count - 1}].accession" class="input-block-level" value="${enzyme.accession}" />
								</td>
								<td class="span2">
									<input type="text" name="enzymes[${status.count - 1}].principalValue" class="input-block-level" value="${enzyme.principalValue}" />
								</td>
								<td class="span1">
									<input type="text" name="enzymes[${status.count - 1}].principalCourtPosition" class="input-block-level" value="${enzyme.principalCourtPosition}" />
								</td>
								<td class="span2">
									<input type="text" name="enzymes[${status.count - 1}].complementValue" class="input-block-level" value="${enzyme.complementValue}" />
								</td>
								<td class="span1">
									<input type="text" name="enzymes[${status.count - 1}].complementCourtPosition" class="input-block-level" value="${enzyme.complementCourtPosition}" />
								</td>
								<td class="span1">
									<button class="btn" onclick="$('#enzyme_${enzyme.accession}').remove()">
										<span class="icon-trash"></span>
									</button>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</form>
		</div>
	</div>

</fieldset>
