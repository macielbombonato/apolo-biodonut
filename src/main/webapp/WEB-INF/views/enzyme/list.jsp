<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<jsp:include page='_search-form.jsp'></jsp:include>

<fieldset>
	<legend>
		<s:message code="enzyme.list.title" />
	</legend>
	
	<table class="table table-striped table-hover table-bordered">
		<thead>
			<tr>
				<th>
					#
				</th>
				<th>
					<s:message code="common.name" />
				</th>
				<th>
					<s:message code="common.accession" />
				</th>
				<th>
					<s:message code="enzyme.codon.principal.value" />
				</th>
				<th>
					<s:message code="enzyme.codon.principal.position" />
				</th>
				<th>
					<s:message code="enzyme.codon.compl.value" />
				</th>
				<th>
					<s:message code="enzyme.codon.compl.position" />
				</th>
				<th>
					<s:message code="common.actions" />
				</th>
			</tr>
		</thead>
		
		<tbody>
			<c:forEach items="${enzymeList}" var="enzyme">
				<tr>
					<td>
						${enzyme.id}
					</td>
					<td>
						${enzyme.name}
					</td>
					<td>
						${enzyme.accession}
					</td>
					<td>
						${enzyme.principalValue}
					</td>
					<td>
						${enzyme.principalCourtPosition}
					</td>
					<td>
						${enzyme.complementValue}
					</td>
					<td>
						${enzyme.complementCourtPosition}
					</td>
					<td>
						<div class="btn-group">
							<a href='<s:url value="/enzyme/edit"></s:url>/${enzyme.id}' class="btn" tabindex="-1">
								<s:message code="common.edit" />
							</a>
							<button class="btn dropdown-toggle" data-toggle="dropdown" tabindex="-1">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href='<s:url value="/enzyme/remove"></s:url>/${enzyme.id}'>
										<s:message code="common.remove" />
									</a>
								</li>
							</ul>
						</div>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</fieldset>
