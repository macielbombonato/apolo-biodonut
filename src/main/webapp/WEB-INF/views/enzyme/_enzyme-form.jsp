<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<input type="hidden" id="id" name="id" value="${enzyme.id}" />

<div class="row">
	<div class="span12">
		<label for="name">
			<s:message code="common.name" />
		</label>
		<input type="text" id="name" name="name" class="input-block-level" value="${enzyme.name}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
</div>
<div class="row">
	<div class="span12">
		<label for="name">
			<s:message code="common.accession" />
		</label>
		<input type="text" id="accession" name="accession" class="input-block-level" value="${enzyme.accession}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
</div>
<div class="row">
	<div class="span6">
		<label for="name">
			<s:message code="enzyme.codon.principal.value" />
		</label>
		<input type="text" id="principalValue" name="principalValue" class="input-block-level" value="${enzyme.principalValue}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
	<div class="span6">
		<label for="name">
			<s:message code="enzyme.codon.principal.position" />
		</label>
		<input type="text" id="principalCourtPosition" name="principalCourtPosition" class="input-block-level" value="${enzyme.principalCourtPosition}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
</div>
<div class="row">
	<div class="span6">
		<label for="name">
			<s:message code="enzyme.codon.compl.value" />
		</label>
		<input type="text" id="complementValue" name="complementValue" class="input-block-level" value="${enzyme.complementValue}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
	<div class="span6">
		<label for="name">
			<s:message code="enzyme.codon.compl.position" />
		</label>
		<input type="text" id="complementCourtPosition" name="complementCourtPosition" class="input-block-level" value="${enzyme.complementCourtPosition}" <c:if test="${readOnly}">readonly="true"</c:if> />
	</div>
</div>