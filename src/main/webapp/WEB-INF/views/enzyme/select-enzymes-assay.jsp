<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<fieldset>
	<legend>
		<s:message code="enzyme.elective.title" />
	</legend>

	<form id="plasmidForm" action="<s:url value="/plasmid/input-data-assay"></s:url>" method="post">
		
		<div class="row">
			<div class="span4">
				<label for="plasmidId">
					<s:message code="plasmid" />
				</label>
				<input type="hidden" id="plasmidId" name="plasmidId" value="${plasmid.id}" readonly="readonly"/>
				<input type="text" id="plasmidAccession" name="plasmidAccession" value="${plasmid.accession}" readonly="readonly"/>
			</div>
			<div class="span4">
				<label for="enzymeInitialId">
					<s:message code="enzyme.initial" />
				</label>
				<input type="text" id="enzymeInitialName" name="enzymeInitialName" readonly="readonly"/>
				<input type="text" id="enzymeInitialAccession" name="enzymeInitialAccession" readonly="readonly"/>
				<input type="hidden" id="enzymeInitialId" name="enzymeInitialId" readonly="readonly"/>
				<input type="hidden" id="enzymePlasmidInitialCourtPositionList" name="enzymePlasmidInitialCourtPositionList" readonly="readonly"/>
				
				<label for="enzymeSequenceInitialCourtPosition">
					<s:message code="enzyme.sequence.court.initial" />
				</label>
				<input type="text" id="enzymeSequenceInitialCourtPosition" name="enzymeSequenceInitialCourtPosition" readonly="readonly"/>
			</div>
			<div class="span4">
				<label for="plasmidId">
					<s:message code="enzyme.final" />
				</label>
				<input type="text" id="enzymeFinalName" name="enzymeFinalName" readonly="readonly"/>
				<input type="text" id="enzymeFinalAccession" name="enzymeFinalAccession" readonly="readonly"/>
				<input type="hidden" id="enzymeFinalId" name="enzymeFinalId" readonly="readonly"/>
				
				<input type="hidden" id="enzymePlasmidFinalCourtPositionList" name="enzymePlasmidFinalCourtPositionList" readonly="readonly"/>
				
				<label for="enzymeSequenceFinalCourtPosition">
					<s:message code="enzyme.sequence.court.final" />
				</label>
				<input type="text" id="enzymeSequenceFinalCourtPosition" name="enzymeSequenceFinalCourtPosition" readonly="readonly"/>
			</div>
			
			<input type="hidden" id="inputName" name="inputName" class="input-block-level" value="${inputedName}" />
			<input type="hidden" id="inputSequence" name="inputSequence" value="${inputedSequence}" />
		</div>
		
		<div class="form-actions">
			<button type="submit" class="btn btn-primary">
				<s:message code="common.ok" /> 
			</button>

			<a href='<s:url value="/"></s:url>' class="btn">
				<s:message code="common.cancel" />
			</a>
		</div>

		<table class="table table-striped table-hover table-bordered">
			<thead>
				<tr>
					<th>
						#
					</th>
					<th>
						<s:message code="common.name" />
					</th>
					<th>
						<s:message code="common.accession" />
					</th>
					<th>
						<s:message code="enzyme.codon.principal.value" />
					</th>
					<th>
						<s:message code="enzyme.codon.principal.position" />
					</th>
					<th>
						<s:message code="enzyme.codon.compl.value" />
					</th>
					<th>
						<s:message code="enzyme.codon.compl.position" />
					</th>
					<th>
						<s:message code="enzyme.initial.label" />
					</th>
					<th>
						<s:message code="enzyme.final.label" />
					</th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach items="${enzymeList}" var="enzyme">
					<input type="hidden" id="enzymeId-${enzyme.id}" value="${enzyme.id}"/>
					
					<tr>
						<td>
							${enzyme.id}
						</td>
						<td>
							${enzyme.name}
						</td>
						<td>
							${enzyme.accession}
						</td>
						<td>
							${enzyme.principalValue}
						</td>
						<td>
							${enzyme.principalCourtPosition}
						</td>
						<td>
							${enzyme.complementValue}
						</td>
						<td>
							${enzyme.complementCourtPosition}
						</td>
						<td>
							<a href='#' class="btn" onclick="chooseInitialEnzyme(${enzyme.id}, ${enzyme.firstSequenceCourtPosition}, ${enzyme.plasmidCourtPosition}, '${enzyme.accession}', '${enzyme.name}');">
								<i class="icon-check" > </i>
							</a>
						</td>
						<td>
							<a href='#' class="btn" onclick="chooseFinalEnzyme(${enzyme.id}, ${enzyme.lastSequenceCourtPosition}, ${enzyme.plasmidCourtPosition}, '${enzyme.accession}', '${enzyme.name}');">
								<i class="icon-check" > </i>
							</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</form>
</fieldset>

<script type="text/javascript">
	function chooseInitialEnzyme(id, courtPosition, plasmidCourtPosition, accession, name) {
		$('#enzymeInitialId').val(id);
		$('#enzymeSequenceInitialCourtPosition').val(courtPosition);
		$('#enzymePlasmidInitialCourtPositionList').val(plasmidCourtPosition);
		$('#enzymeInitialAccession').val(accession);
		$('#enzymeInitialName').val(accession);
	}
	
	function chooseFinalEnzyme(id, courtPosition, plasmidCourtPosition, accession, name) {
		$('#enzymeFinalId').val(id);
		$('#enzymeSequenceFinalCourtPosition').val(courtPosition);
		$('#enzymePlasmidFinalCourtPositionList').val(plasmidCourtPosition);
		$('#enzymeFinalAccession').val(accession);
		$('#enzymeFinalName').val(accession);
	}
</script>