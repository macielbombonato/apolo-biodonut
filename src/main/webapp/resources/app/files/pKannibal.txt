LOCUS       AJ311873                6063 bp    DNA     circular SYN 14-NOV-2006
DEFINITION  Cloning vector pKANNIBAL.
ACCESSION   AJ311873
VERSION     AJ311873.1  GI:15982216
KEYWORDS    kan gene; kanamycin resistance protein; pdk gene; promoter.
SOURCE      Cloning vector pKANNIBAL
  ORGANISM  Cloning vector pKANNIBAL
            other sequences; artificial sequences; vectors.
REFERENCE   1
  AUTHORS   Wesley,S.V., Helliwell,C.A., Smith,N.A., Wang,M.B., Rouse,D.T.,
            Liu,Q., Gooding,P.S., Singh,S.P., Abbott,D., Stoutjesdijk,P.A.,
            Robinson,S.P., Gleave,A.P., Green,A.G. and Waterhouse,P.M.
  TITLE     Construct design for efficient, effective and high-throughput gene
            silencing in plants
  JOURNAL   Plant J. 27 (6), 581-590 (2001)
   PUBMED   11576441
REFERENCE   2  (bases 1 to 6063)
  AUTHORS   Waterhouse,P.M.
  TITLE     Direct Submission
  JOURNAL   Submitted (04-MAY-2001) Waterhouse P.M., Plant Industry,
            C.S.I.R.O., P.O. Box 1600, Canberra, ACT 2601, AUSTRALIA
FEATURES             Location/Qualifiers
     source          1..6063
                     /organism="Cloning vector pKANNIBAL"
                     /mol_type="other DNA"
                     /db_xref="taxon:167048"
                     /lab_host="Escherichia coli"
                     /focus
                     /note="pHANNIBAL is a derivative of cloning vector pART7
                     which was a derivative of pGEM-9Zf(-)"
     source          1..3103
                     /organism="Escherichia coli K-12"
                     /mol_type="other DNA"
                     /strain="K-12"
                     /db_xref="taxon:83333"
     source          3104..4449
                     /organism="Cauliflower mosaic virus"
                     /mol_type="other DNA"
                     /db_xref="taxon:10641"
     source          4493..5234
                     /organism="Flaveria trinervia"
                     /mol_type="other DNA"
                     /db_xref="taxon:4227"
     source          5288..6053
                     /organism="Agrobacterium tumefaciens"
                     /mol_type="other DNA"
                     /db_xref="taxon:358"
     gene            1075..1869
                     /gene="kan"
     CDS             1075..1869
                     /gene="kan"
                     /codon_start=1
                     /transl_table=11
                     /product="kanamycin resistance protein"
                     /protein_id="CAC86251.1"
                     /db_xref="GI:15982217"
                     /translation="MAKMRISPELKKLIEKYRCVKDTEGMSPAKVYKLVGENENLYLK
                     MTDSRYKGTTYDVEREKDMMLWLEGKLPVPKVLHFERHDGWSNLLMSEADGVLCSEEY
                     EDEQSPEKIIELYAECIRLFHSIDISDCPYTNSLDSRLAELDYLLNNDLADVDCENWE
                     EDTPFKDPRELYDFLKTEKPEEELVFSHGDLGDSNIFVKDGKVSGFIDLGRSGRADKW
                     YDIAFCVRSIREDIGEEQYVELFFDLLGIKPDWEKIKYYILLDELF"
     promoter        3104..4449
                     /function="35S promoter"
     gene            4493..5234
                     /gene="pdk"
     intron          4493..5234
                     /gene="pdk"
                     /note="from pyruvate orthophosphate dikinase (pdk)"
                     /number=2
     terminator      5288..6053
                     /note="octopine esynthase (ocs) terminator"
ORIGIN      
        1 tgaccaagtc agcttggcac tggccgtcgt tttacaacgt cgtgactggg aaaaccctgg
       61 cgttacccaa cttaatcgcc ttgcagcaca tccccctttc gccagctggc gtaatagcga
      121 agaggcccgc accgatcgcc cttcccaaca gttgcgcagc ctgaatggcg aatgggaaat
      181 tgtaaacgtt aatattttgt taatattttg ttaaaattcg cgttaaattt ttgttaaatc
      241 agctcatttt ttaaccaata ggccgaaatc ggcaaaatcc cttataaatc aaaagaatag
      301 accgagatag ggttgagtgt tgttccagtt tggaacaaga gtccactatt aaagaacgtg
      361 gactccaacg tcaaagggcg aaaaaccgtc tatcagggcg atggcccact acgtgaacca
      421 tcaccctaat caagtttttt ggggtcgagg tgccgtaaag cactaaatcg gaaccctaaa
      481 gggatgcccc gatttagagc ttgacgggga aagccggcga acgtggcgag aaaggaaggg
      541 aagaaagcga aaggagcggg cgctagggcg ctggcaagtg tagcggtcac gctgcgcgta
      601 accaccacac ccgccgcgct taatgcgccg ctacagggcg cgtcaggtgg cacttttcgg
      661 ggaaatgtgc gcggaacccc tatttgttta tttttctaaa tacattcaaa tatgtatccg
      721 ctcatgagac aataaccctg ataaatgctt caataatatt gaaaaaggaa gagtatgagt
      781 attcaacatt tccgtgtcgc ccttattccc ttttttgcgg cattttgcct tcctgttttt
      841 gctcacccag aaacgctggt gaaagtaaaa gatgctgaag atcagttggg tgcacgagtg
      901 ggttacatcg aactggatct caacagcggt aagatccttg agagttttcg ccccgaagaa
      961 cgttttccaa tgatgagcac tttttgcaag gaacagtgaa ttggagttcg tcttgttata
     1021 attagcttct tggggtatct ttaaatactg tagaaaagag gaaggaaata ataaatggct
     1081 aaaatgagaa tatcaccgga attgaaaaaa ctgatcgaaa aataccgctg cgtaaaagat
     1141 acggaaggaa tgtctcctgc taaggtatat aagctggtgg gagaaaatga aaacctatat
     1201 ttaaaaatga cggacagccg gtataaaggg accacctatg atgtggaacg ggaaaaggac
     1261 atgatgctat ggctggaagg aaagctgcct gttccaaagg tcctgcactt tgaacggcat
     1321 gatggctgga gcaatctgct catgagtgag gccgatggcg tcctttgctc ggaagagtat
     1381 gaagatgaac aaagccctga aaagattatc gagctgtatg cggagtgcat caggctcttt
     1441 cactccatcg acatatcgga ttgtccctat acgaatagct tagacagccg cttagccgaa
     1501 ttggattact tactgaataa cgatctggcc gatgtggatt gcgaaaactg ggaagaagac
     1561 actccattta aagatccgcg cgagctgtat gattttttaa agacggaaaa gcccgaagag
     1621 gaacttgtct tttcccacgg cgacctggga gacagcaaca tctttgtgaa agatggcaaa
     1681 gtaagtggct ttattgatct tgggagaagc ggcagggcgg acaagtggta tgacattgcc
     1741 ttctgcgtcc ggtcgatcag ggaggatatc ggggaagaac agtatgtcga gctatttttt
     1801 gacttactgg ggatcaagcc tgattgggag aaaataaaat attatatttt actggatgaa
     1861 ttgttttagt acctagaatg catgaccaaa atcccttaac gtgagttttc gttccactga
     1921 gcgtcagacc ccgtaaaagg atctaggtga agatcctttt tgataatctc atgaccaaaa
     1981 tcccttaacg tgagttttcg ttccactgag cgtcagaccc cgtagaaaag atcaaaggat
     2041 cttcttgaga tccttttttt ctgcgcgtaa tctgctgctt gcaaacaaaa aaaccaccgc
     2101 taccagcggt ggtttgtttg ccggatcaag agctaccaac tctttttccg aaggtaactg
     2161 gcttcagcag agcgcagata ccaaatactg tccttctagt gtagccgtag ttaggccacc
     2221 acttcaagaa ctctgtagca ccgcctacat acctcgctct gctaatcctg ttaccagtgg
     2281 ctgctgccag tggcgataag tcgtgtctta ccgggttgga ctcaagacga tagttaccgg
     2341 ataaggcgca gcggtcgggc tgaacggggg gttcgtgcac acagcccagc ttggagcgaa
     2401 cgacctacac cgaactgaga tacctacagc gtgagctatg agaaagcgcc acgcttcccg
     2461 aagggagaaa ggcggacagg tatccggtaa gcggcagggt cggaacagga gagcgcacga
     2521 gggagcttcc agggggaaac gcctggtatc tttatagtcc tgtcgggttt cgccacctct
     2581 gacttgagcg tcgatttttg tgatgctcgt caggggggcg gagcctatgg aaaaacgcca
     2641 gcaacgcggc ctttttacgg ttcctggcct tttgctggcc ttttgctcac atgttctttc
     2701 ctgcgttatc ccctgattct gtggataacc gtattaccgc ctttgagtga gctgataccg
     2761 ctcgccgcag ccgaacgacc gagcgcagcg agtcagtgag cgaggaagcg gaagagcgcc
     2821 caatacgcaa accgcctctc cccgcgcgtt ggccgattca ttaatgcagc tggcacgaca
     2881 ggtttcccga ctggaaagcg ggcagtgagc gcaacgcaat taatgtgagt tagctcactc
     2941 attaggcacc ccaggcttta cactttatgc ttccggctcg tatgttgtgt ggaattgtga
     3001 gcggataaca atttcacaca ggaaacagct atgaccatga ttacgaattt ggccaagtcg
     3061 gcctctaata cgactcacta tagggagctc gtcgagcggc cgctcgacga attaattcca
     3121 atcccacaaa aatctgagct taacagcaca gttgctcctc tcagagcaga atcgggtatt
     3181 caacaccctc atatcaacta ctacgttgtg tataacggtc cacatgccgg tatatacgat
     3241 gactggggtt gtacaaaggc ggcaacaaac ggcgttcccg gagttgcaca caagaaattt
     3301 gccactatta cagaggcaag agcagcagct gacgcgtaca caacaagtca gcaaacagac
     3361 aggttgaact tcatccccaa aggagaagct caactcaagc ccaagagctt tgctaaggcc
     3421 ctaacaagcc caccaaagca aaaagcccac tggctcacgc taggaaccaa aaggcccagc
     3481 agtgatccag ccccaaaaga gatctccttt gccccggaga ttacaatgga cgatttcctc
     3541 tatctttacg atctaggaag gaagttcgaa ggtgaaggtg acgacactat gttcaccact
     3601 gataatgaga aggttagcct cttcaatttc agaaagaatg ctgacccaca gatggttaga
     3661 gaggcctacg cagcaggtct catcaagacg atctacccga gtaacaatct ccaggagatc
     3721 aaataccttc ccaagaaggt taaagatgca gtcaaaagat tcaggactaa ttgcatcaag
     3781 aacacagaga aagacatatt tctcaagatc agaagtacta ttccagtatg gacgattcaa
     3841 ggcttgcttc ataaaccaag gcaagtaata gagattggag tctctaaaaa ggtagttcct
     3901 actgaatcta aggccatgca tggagtctaa gattcaaatc gaggatctaa cagaactcgc
     3961 cgtgaagact ggcgaacagt tcatacagag tcttttacga ctcaatgaca agaagaaaat
     4021 cttcgtcaac atggtggagc acgacactct ggtctactcc aaaaatgtca aagatacagt
     4081 ctcagaagac caaagggcta ttgagacttt tcaacaaagg ataatttcgg gaaacctcct
     4141 cggattccat tgcccagcta tctgtcactt catcgaaagg acagtagaaa aggaaggtgg
     4201 ctcctacaaa tgccatcatt gcgataaagg aaaggctatc attcaagatc tctctgccga
     4261 cagtggtccc aaagatggac ccccacccac gaggagcatc gtggaaaaag aagacgttcc
     4321 aaccacgtct tcaaagcaag tggattgatg tgacatctcc actgacgtaa gggatgacgc
     4381 acaatcccac tatccttcgc aagacccttc ctctatataa ggaagttcat ttcatttgga
     4441 gaggacacgc tcgaggaatt cggtacccca attggtaagg aaataattat tttctttttt
     4501 ccttttagta taaaatagtt aagtgatgtt aattagtatg attataataa tatagttgtt
     4561 ataattgtga aaaaataatt tataaatata ttgtttacat aaacaacata gtaatgtaaa
     4621 aaaatatgac aagtgatgtg taagacgaag aagataaaag ttgagagtaa gtatattatt
     4681 tttaatgaat ttgatcgaac atgtaagatg atatactagc attaatattt gttttaatca
     4741 taatagtaat tctagctggt ttgatgaatt aaatatcaat gataaaatac tatagtaaaa
     4801 ataagaataa ataaattaaa ataatatttt tttatgatta atagtttatt atataattaa
     4861 atatctatac cattactaaa tattttagtt taaaagttaa taaatatttt gttagaaatt
     4921 ccaatctgct tgtaatttat caataaacaa aatattaaat aacaagctaa agtaacaaat
     4981 aatatcaaac taatagaaac agtaatctaa tgtaacaaaa cataatctaa tgctaatata
     5041 acaaagcgca agatctatca ttttatatag tattattttc aatcaacatt cttattaatt
     5101 tctaaataat acttgtagtt ttattaactt ctaaatggat tgactattaa ttaaatgaat
     5161 tagtcgaaca tgaataaaca aggtaacatg atagatcatg tcattgtgtt atcattgatc
     5221 ttacatttgg attgattaca gttgggaaat tgggttcgaa atcgataagc ttggatcctc
     5281 tagagtcctg ctttaatgag atatgcgaga cgcctatgat cgcatgatat ttgctttcaa
     5341 ttctgttgtg cacgttgtaa aaaacctgag catgtgtagc tcagatcctt accgccggtt
     5401 tcggttcatt ctaatgaata tatcacccgt tactatcgta tttttatgaa taatattctc
     5461 cgttcaattt actgattgta ccctactact tatatgtaca atattaaaat gaaaacaata
     5521 tattgtgctg aataggttta tagcgacatc tatgatagag cgccacaata acaaacaatt
     5581 gcgttttatt attacaaatc caattttaaa aaaagcggca gaaccggtca aacctaaaag
     5641 actgattaca taaatcttat tcaaatttca aaaggcccca ggggctagta tctacgacac
     5701 accgagcggc gaactaataa cgttcactga agggaactcc ggttccccgc cggcgcgcat
     5761 gggtgagatt ccttgaagtt gagtattggc cgtccgctct accgaaagtt acgggcacca
     5821 ttcaacccgg tccagcacgg cggccgggta accgacttgc tgccccgaga attatgcagc
     5881 atttttttgg tgtatgtggg ccccaaatga agtgcaggtc aaaccttgac agtgacgaca
     5941 aatcgttggg cgggtccagg gcgaattttg cgacaacatg tcgaggctca gcaggacctg
     6001 caggcatgca agctagctta ctagtgatgc atattctata gtgtcaccta aatctgcggc
     6061 cgc
//
