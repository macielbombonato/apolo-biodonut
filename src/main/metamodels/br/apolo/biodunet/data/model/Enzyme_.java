package br.apolo.biodunet.data.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Enzyme.class)
public abstract class Enzyme_ extends br.apolo.data.model.BaseEntity_ {

	public static volatile SingularAttribute<Enzyme, String> name;
	public static volatile SingularAttribute<Enzyme, String> accession;

}

