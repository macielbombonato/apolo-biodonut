package br.apolo.biodunet.data.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Plasmid.class)
public abstract class Plasmid_ extends br.apolo.data.model.BaseEntity_ {

	public static volatile SingularAttribute<Plasmid, String> accession;
	public static volatile SingularAttribute<Plasmid, String> locus;
	public static volatile SingularAttribute<Plasmid, String> definition;
	public static volatile SingularAttribute<Plasmid, String> keywords;
	public static volatile SingularAttribute<Plasmid, String> source;

}

