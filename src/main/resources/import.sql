INSERT INTO user_group (name) VALUES ('Administrators');
INSERT INTO user_group (name) VALUES ('Super Users');
INSERT INTO user_group (name) VALUES ('Users');

INSERT INTO group_permission (group_id, permission_name) VALUES (1, 'ADMIN'); 

INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER'); 
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER_CREATE'); 
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER_EDIT');
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER_REMOVE');
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER_LIST');
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER_PERMISSION_LIST');
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER_PERMISSION_CREATE');
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER_PERMISSION_EDIT');
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER_PERMISSION_REMOVE');
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'USER_PERMISSION_VIEW');
INSERT INTO group_permission (group_id, permission_name) VALUES (2, 'DATA_MANAGER');

INSERT INTO group_permission (group_id, permission_name) VALUES (3, 'USER');
INSERT INTO group_permission (group_id, permission_name) VALUES (3, 'DATA_MANAGER');

INSERT INTO user (name, email, password) VALUES ('Maciel Escudero Bombonato', 'maciel.bombonato@gmail.com', '{SHA}vQAbnXx6h5GRcfoV7qPvucCSGa8=');
INSERT INTO user (name, email, password) VALUES ('Marco Aurelio Takita', 'takita@centrodecitricultura.br', '{SHA}d5qSPWmy4HJ0exGXW6hpSd4WcDc=');
INSERT INTO user (name, email, password) VALUES ('William Paiva', 'will.unicamp@gmail.com', '{SHA}fEqNCco3Yq9h5ZUglD3CZJT4lBs=');
INSERT INTO user (name, email, password) VALUES ('Eloa Carolina', 'elo.carol@gmail.com', '{SHA}d5qSPWmy4HJ0exGXW6hpSd4WcDc=');

INSERT INTO users_in_groups (user_id, group_id) VALUES (1, 1);
INSERT INTO users_in_groups (user_id, group_id) VALUES (2, 1);
INSERT INTO users_in_groups (user_id, group_id) VALUES (3, 1);
INSERT INTO users_in_groups (user_id, group_id) VALUES (4, 1);
