package br.apolo.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.apolo.web.enums.Navigation;

@Controller
public class IndexController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		return Navigation.INDEX.getPath();
	}
	
	@RequestMapping(value = "/help/team", method = RequestMethod.GET)
	public String team(Model model) {
		return Navigation.HELP_TEAM.getPath();
	}
	
	@RequestMapping(value = "/help/howto", method = RequestMethod.GET)
	public String howto(Model model) {
		return Navigation.HELP_HOW_TO.getPath();
	}
	
	@RequestMapping(value = "/help/portal", method = RequestMethod.GET)
	public String portal(Model model) {
		return Navigation.HELP_PORTAL.getPath();
	}
}
