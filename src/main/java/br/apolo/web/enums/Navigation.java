package br.apolo.web.enums;

public enum Navigation {
	
	HOME("/"), 
	INDEX("index"),
	
	HELP_TEAM("help/team"),
	HELP_HOW_TO("help/howto"),
	HELP_PORTAL("help/portal"),
	
	USER("user"),
	USER_INDEX("user/index"),
	USER_NEW("user/new"),
	USER_EDIT("user/edit"),
	USER_LIST("user/list"),
	USER_SEARCH("user/search"),
	USER_VIEW("user/view"),
	USER_CHANGE_PASSWORD("user/change-password"),
	
	USER_PERMISSION_LIST("user-group/list"),
	USER_PERMISSION_SEARCH("user-group/search"),
	USER_PERMISSION_CREATE("user-group/new"),
	USER_PERMISSION_EDIT("user-group/edit"),
	USER_PERMISSION_VIEW("user-group/view"),
	
	
	AUTH("auth"),
	AUTH_LOGIN("auth/login"),
	AUTH_LOGOUT("auth/logout"),
	
	ERROR("error/error"),
	
	ENZYME_LIST("enzyme/list"),
	ENZYME_ELECTIVE("enzyme/select-enzymes-assay"),
	ENZYME_SEARCH("enzyme/search"),
	ENZYME_NEW("enzyme/new"),
	ENZYME_EDIT("enzyme/edit"),
	ENZYME_NEW_BATCH("enzyme/new-batch"),
	ENZYME_NEW_BATCH_APPROVE_LIST("enzyme/new-batch-approve-list"),
	
	PLASMID_VIEW("plasmid/view"),
	PLASMID_LIST("plasmid/list"),
	PLASMID_INPUT_DATA_ASSAY("plasmid/input-data-assay"),
	PLASMID_EDIT("plasmid/edit"),
	PLASMID_SEARCH("plasmid/search"),
	PLASMID_NEW("plasmid/new"),
	PLASMID_NEW_APPROVE("plasmid/new-approve"),
	
	;
	
	
	private String path;

	private Navigation(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}

}
