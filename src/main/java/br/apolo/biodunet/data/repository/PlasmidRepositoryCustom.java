package br.apolo.biodunet.data.repository;

import br.apolo.biodunet.data.model.Plasmid;
import br.apolo.data.repository.BaseRepository;

public interface PlasmidRepositoryCustom extends BaseRepository<Plasmid> {

}
