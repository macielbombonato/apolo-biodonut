package br.apolo.biodunet.data.repository;

import br.apolo.biodunet.data.model.Enzyme;
import br.apolo.data.repository.BaseRepository;

public interface EnzymeRepositoryCustom extends BaseRepository<Enzyme> {

}
