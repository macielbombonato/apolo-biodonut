package br.apolo.biodunet.data.repository.impl;

import org.springframework.stereotype.Repository;

import br.apolo.biodunet.data.model.Enzyme;
import br.apolo.biodunet.data.repository.EnzymeRepositoryCustom;
import br.apolo.data.repository.impl.BaseRepotitoryImpl;

@Repository
public class EnzymeRepositoryImpl extends BaseRepotitoryImpl<Enzyme> implements EnzymeRepositoryCustom {
	
}
