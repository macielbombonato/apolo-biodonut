package br.apolo.biodunet.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.apolo.biodunet.data.model.Plasmid;

public interface PlasmidRepository extends CrudRepository<Plasmid, Long>, PlasmidRepositoryCustom {

	@Query("FROM Plasmid e ORDER BY e.accession")
	List<Plasmid> findAllPlasmids();
	
}
