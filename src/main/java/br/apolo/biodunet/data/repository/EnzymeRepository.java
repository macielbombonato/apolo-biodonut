package br.apolo.biodunet.data.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.apolo.biodunet.data.model.Enzyme;

public interface EnzymeRepository extends CrudRepository<Enzyme, Long>, EnzymeRepositoryCustom {
	
	@Query("FROM Enzyme e ORDER BY e.name")
	List<Enzyme> findAllEnzymes();
	
}
