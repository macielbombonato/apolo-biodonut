package br.apolo.biodunet.data.repository.impl;

import org.springframework.stereotype.Repository;

import br.apolo.biodunet.data.model.Plasmid;
import br.apolo.biodunet.data.repository.PlasmidRepositoryCustom;
import br.apolo.data.repository.impl.BaseRepotitoryImpl;

@Repository
public class PlasmidRepositoryImpl extends BaseRepotitoryImpl<Plasmid> implements PlasmidRepositoryCustom {
	
}
