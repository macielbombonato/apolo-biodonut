package br.apolo.biodunet.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.apolo.biodunet.data.enums.DirectionEnum;
import br.apolo.data.model.BaseEntity;

@Entity
@Table(name = "feature")
public class Feature extends BaseEntity {
	
	private static final long serialVersionUID = -607232680281076309L;

	@Column
	private String name;
	
	@Column
	private Long startPosition;
	
	@Column
	private Long endPosition;
	
	@Column
	@Enumerated
	private DirectionEnum direction;
	
	@Column(length=4000)
	private String details;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plasmid_id", nullable = false)
	private Plasmid plasmid;
	
	@Transient
	private Integer color;
	
	@Transient
	private boolean highlight;
	
	@Transient
	private boolean enzymeFeature;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(Long startPosition) {
		this.startPosition = startPosition;
	}

	public Long getEndPosition() {
		return endPosition;
	}

	public void setEndPosition(Long endPosition) {
		this.endPosition = endPosition;
	}

	public Plasmid getPlasmid() {
		return plasmid;
	}

	public void setPlasmid(Plasmid plasmid) {
		this.plasmid = plasmid;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	@Transient
	public Long getSize() {
		Long size = 0L;
		
		if (startPosition != null && endPosition != null) {
			size = endPosition - startPosition;
		}
		
		return size;
	}

	public Integer getColor() {
		return color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public boolean isHighlight() {
		return highlight;
	}

	public void setHighlight(boolean highlight) {
		this.highlight = highlight;
	}

	public DirectionEnum getDirection() {
		return direction;
	}

	public void setDirection(DirectionEnum direction) {
		this.direction = direction;
	}

	public boolean isEnzymeFeature() {
		return enzymeFeature;
	}

	public void setEnzymeFeature(boolean enzymeFeature) {
		this.enzymeFeature = enzymeFeature;
	}
}
