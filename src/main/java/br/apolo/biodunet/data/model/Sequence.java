package br.apolo.biodunet.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import br.apolo.data.model.BaseEntity;

@Entity
@Table(name = "sequence")
@NamedQueries({
	@NamedQuery(name="Sequence.getAll", 
			query=" select t " +
					" from Sequence t " +
					" where t.plasmid.id = :plasmid_id " +
					" order by t.sequenceInitialPosition ")
})
public class Sequence extends BaseEntity {
	
	private static final long serialVersionUID = -282356250664780523L;

	@Column
	private Long sequenceInitialPosition;
	
	@Column(length=10, nullable=false)
	private String value;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plasmid_id", nullable = false)
	private Plasmid plasmid;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Plasmid getPlasmid() {
		return plasmid;
	}

	public void setPlasmid(Plasmid plasmid) {
		this.plasmid = plasmid;
	}

	public Long getSequenceInitialPosition() {
		return sequenceInitialPosition;
	}

	public void setSequenceInitialPosition(Long sequenceInitialPosition) {
		this.sequenceInitialPosition = sequenceInitialPosition;
	}

}
