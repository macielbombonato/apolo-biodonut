package br.apolo.biodunet.data.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.apolo.data.model.AuditableBaseEntity;

@Entity
@Table(name = "enzyme")
public class Enzyme extends AuditableBaseEntity {
	
	private static final long serialVersionUID = 5751401566413965861L;

	@Column(unique = true, nullable = false)
	private String name;
	
	@Column(unique = true, nullable = false)
	private String accession;
	
	@Column
	private String principalValue;
	
	@Column
	private Long principalCourtPosition;
	
	@Column
	private String complementValue;
	
	@Column
	private Long complementCourtPosition;
	
	@Transient
	private List<String> plasmidCourtPosition;
	
	@Transient
	private List<Long> sequenceCourtPosition;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPrincipalValue() {
		return principalValue;
	}

	public void setPrincipalValue(String principalValue) {
		this.principalValue = principalValue;
	}

	public Long getPrincipalCourtPosition() {
		return principalCourtPosition;
	}

	public void setPrincipalCourtPosition(Long principalCourtPosition) {
		this.principalCourtPosition = principalCourtPosition;
	}

	public String getComplementValue() {
		return complementValue;
	}

	public void setComplementValue(String complementValue) {
		this.complementValue = complementValue;
	}

	public Long getComplementCourtPosition() {
		return complementCourtPosition;
	}

	public void setComplementCourtPosition(Long complementCourtPosition) {
		this.complementCourtPosition = complementCourtPosition;
	}

	public String getAccession() {
		return accession;
	}

	public void setAccession(String accession) {
		this.accession = accession;
	}

	public List<String> getPlasmidCourtPosition() {
		return plasmidCourtPosition;
	}

	public void setPlasmidCourtPosition(List<String> plasmidCourtPosition) {
		this.plasmidCourtPosition = plasmidCourtPosition;
	}

	public List<Long> getSequenceCourtPosition() {
		return sequenceCourtPosition;
	}

	public void setSequenceCourtPosition(List<Long> sequenceCourtPosition) {
		this.sequenceCourtPosition = sequenceCourtPosition;
	}
	
	public Long getFirstSequenceCourtPosition() {
		Long result = 0L;
		
		if (this.sequenceCourtPosition != null && !this.sequenceCourtPosition.isEmpty()) {
			result = this.sequenceCourtPosition.get(0);
		}
		
		return result;
	}
	
	public Long getLastSequenceCourtPosition() {
		Long result = 0L;
		
		if (this.sequenceCourtPosition != null && !this.sequenceCourtPosition.isEmpty()) {
			result = this.sequenceCourtPosition.get(this.sequenceCourtPosition.size() - 1);
		}
		
		return result;
	}

}
