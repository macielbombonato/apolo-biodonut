package br.apolo.biodunet.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.apolo.biodunet.business.service.EnzymeService;
import br.apolo.biodunet.business.service.PlasmidService;
import br.apolo.biodunet.data.helper.FileNCBIHelper;
import br.apolo.biodunet.data.model.Enzyme;
import br.apolo.biodunet.web.model.EnzymesFormModel;
import br.apolo.biodunet.web.model.FileFormModel;
import br.apolo.business.model.SearchResult;
import br.apolo.common.util.MessageBundle;
import br.apolo.security.SecuredEnum;
import br.apolo.security.UserPermission;
import br.apolo.web.controller.BaseController;
import br.apolo.web.enums.Navigation;

@Controller
@RequestMapping(value = "/enzyme")
public class EnzymeController extends BaseController<Enzyme> {

	@Autowired
	PlasmidService plasmidService;
	
	@Autowired
	EnzymeService enzymeService;

	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView(Navigation.ENZYME_LIST.getPath());

		List<Enzyme> enzymeList = enzymeService.list();

		mav.addObject("enzymeList", enzymeList);

		return mav;
	}

	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(Enzyme entity) {
		ModelAndView mav = new ModelAndView();

		if (entity != null) {
			enzymeService.save(entity);

			mav = list();

			mav.addObject("msg", true);
			mav.addObject("message",
					MessageBundle.getMessageBundle("common.msg.save.success"));
		}

		return mav;
	}
	
	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "save-batch", method = RequestMethod.POST)
	public ModelAndView saveBatch(@ModelAttribute("enzymes") EnzymesFormModel enzymes, Model map) {
		ModelAndView mav = new ModelAndView();

		boolean hasErrors = false;
		Integer successCount = 0;
		Integer errorCount = 0;
		
		if (enzymes != null && enzymes.getEnzymes() != null && !enzymes.getEnzymes().isEmpty()) {
			for (Enzyme enzyme : enzymes.getEnzymes()) {
				if (enzyme != null && enzyme.getAccession() != null && enzyme.getName() != null) {
					try {
						enzymeService.save(enzyme);
						successCount++;
					} catch (Exception e) {
						errorCount++;
						hasErrors = true;
					}
				}
			}
			
			mav = list();

			if (hasErrors) {
				if (successCount == 0) {
					mav.addObject("error", true);	
				} else {
					mav.addObject("warn", true);
				}
				
				mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.results", successCount, errorCount));
			} else {
				mav.addObject("msg", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.all.success", successCount));
			}
		}

		return mav;
	}

	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "new-batch", method = RequestMethod.GET)
	public ModelAndView createBatch() {
		ModelAndView mav = new ModelAndView(Navigation.ENZYME_NEW_BATCH.getPath());
		return mav;
	}

	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "new-batch-proccess-file", method = RequestMethod.POST)
	public ModelAndView createBatchProccessFile(
			@ModelAttribute("uploadForm") FileFormModel uploadForm,
					Model map) {
		
		ModelAndView mav = new ModelAndView(Navigation.ENZYME_NEW_BATCH_APPROVE_LIST.getPath());
		
		List<MultipartFile> files = uploadForm.getFiles();

		List<String> fileNames = new ArrayList<String>();
		
		List<Enzyme> enzymes = new ArrayList<Enzyme>();
		
		if(null != files && files.size() > 0) {
			for (MultipartFile multipartFile : files) {

				String fileName = multipartFile.getOriginalFilename();
				fileNames.add(fileName);
				
	    		if(multipartFile != null) {
	    			FileNCBIHelper helper = new FileNCBIHelper();
	    			
	        		try {
						enzymes = helper.enzymesFileConverter(multipartFile);
						mav.addObject("enzymeList", enzymes);
					} catch (IOException e) {
						e.printStackTrace();
					}
	                
	    		}
			}
		}
		
		return mav;
	}
	
	@Override
	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "new", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView mav = new ModelAndView(Navigation.ENZYME_NEW.getPath());

		mav.addObject("enzyme", new Enzyme());
		mav.addObject("readOnly", false);

		return mav;
	}

	@Override
	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable Long id) {
		ModelAndView mav = new ModelAndView(Navigation.ENZYME_EDIT.getPath());
		
		Enzyme enzyme = enzymeService.find(id);
		
		mav.addObject("enzyme", enzyme);
		mav.addObject("readOnly", false);
		
		return mav;
	}

	@Override
	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "remove/{id}", method = RequestMethod.GET)
	public ModelAndView remove(@PathVariable Long id) {
		ModelAndView mav = new ModelAndView(Navigation.ENZYME_LIST.getPath());
		
		Enzyme enzyme = enzymeService.find(id);
		
		if (enzyme != null) {
			enzymeService.remove(enzyme);
			
			mav = list();
			
			mav.addObject("msg", true);
			mav.addObject("message", MessageBundle.getMessageBundle("common.msg.remove.success"));
		} else {
			mav = list();
			
			mav.addObject("error", true);
			mav.addObject("message", MessageBundle.getMessageBundle("common.msg.remove.fail"));
		}
		
		return mav;
	}
	
	@RequestMapping(value = "search-form", method = RequestMethod.GET)
	public ModelAndView searchForm() {
		ModelAndView mav = new ModelAndView(Navigation.ENZYME_SEARCH.getPath());
		
		return mav;
	}
	
	@RequestMapping(value = "search", method = RequestMethod.POST)
	public ModelAndView search(@ModelAttribute("param") String param) {
		ModelAndView mav = new ModelAndView(Navigation.ENZYME_LIST.getPath());
		
		SearchResult<Enzyme> result = enzymeService.search(param);
		
		List<Enzyme> enzymeList = result.getResults();
		
		mav.addObject("enzymeList", enzymeList);
		
		return mav;
	}

	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		dataBinder.setAutoGrowCollectionLimit(Integer.MAX_VALUE);
	}
}
