package br.apolo.biodunet.web.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.apolo.biodunet.business.service.EnzymeService;
import br.apolo.biodunet.business.service.PlasmidService;
import br.apolo.biodunet.business.service.impl.PlasmidDrawService;
import br.apolo.biodunet.data.helper.FileNCBIHelper;
import br.apolo.biodunet.data.model.Enzyme;
import br.apolo.biodunet.data.model.Plasmid;
import br.apolo.biodunet.web.model.FileFormModel;
import br.apolo.biodunet.web.model.PlasmidAssayFormModel;
import br.apolo.business.model.SearchResult;
import br.apolo.common.util.MessageBundle;
import br.apolo.security.SecuredEnum;
import br.apolo.security.UserPermission;
import br.apolo.web.controller.BaseController;
import br.apolo.web.enums.Navigation;

@Controller
@RequestMapping(value = "/plasmid")
public class PlasmidController extends BaseController<Plasmid> {

	@Autowired
	PlasmidService plasmidService;
	
	@Autowired
	EnzymeService enzymeService;
	
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_LIST.getPath());

		List<Plasmid> plasmidList = plasmidService.list();

		mav.addObject("plasmidList", plasmidList);

		return mav;
	}

	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("plasmid") Plasmid entity) {
		ModelAndView mav = new ModelAndView();

		if (entity != null) {
			try {
				plasmidService.save(entity);
				
				mav = list();

				mav.addObject("msg", true);
				mav.addObject("message", MessageBundle.getMessageBundle("common.msg.save.success"));
			} catch (Exception e) {
				mav = list();

				mav.addObject("error", true);
				mav.addObject("message", e.getMessage());
			}
		}

		return mav;
	}

	@Override
	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "new", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_NEW.getPath());

		mav.addObject("plasmid", new Plasmid());
		mav.addObject("readOnly", false);

		return mav;
	}
	
	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "new-proccess-file", method = RequestMethod.POST)
	public ModelAndView createProccessFile(@ModelAttribute("uploadForm") FileFormModel uploadForm, Model map) {
		
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_NEW_APPROVE.getPath());
		
		List<MultipartFile> files = uploadForm.getFiles();

		List<String> fileNames = new ArrayList<String>();
		
		Plasmid plasmid = new Plasmid();
		
		if(null != files && files.size() > 0) {
			for (MultipartFile multipartFile : files) {

				String fileName = multipartFile.getOriginalFilename();
				fileNames.add(fileName);
				
	    		if(multipartFile != null) {
	    			FileNCBIHelper helper = new FileNCBIHelper();
	    			
	        		try {
	        			plasmid = helper.plasmidFileConverter(multipartFile);
	        			
	        			getPlasmidDunetChart(plasmid);
	        			
						mav.addObject("plasmid", plasmid);
					} catch (IOException e) {
						e.printStackTrace();
					}
	    		}
			}
		}
		
		return mav;
	}
	
	private void getPlasmidDunetChart(Plasmid plasmid) {
		if (plasmid != null) {
			try {
				PlasmidDrawService drawService = new PlasmidDrawService();
				
				drawService.setPlasmid(plasmid);
				drawService.draw();
				
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				ImageIO.write(drawService.getImage(), "png", os);
				
				plasmid.setPlasmidDunetChart(os.toByteArray());
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@RequestMapping(value = "view/{id}", method = RequestMethod.GET)
	public ModelAndView view(@PathVariable Long id) {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_VIEW.getPath());
		
		Plasmid plasmid = plasmidService.find(id);
		
		if (plasmid == null) {
			mav = list();
			
			mav.addObject("error", true);
			mav.addObject("message", MessageBundle.getMessageBundle("plasmid.msg.not.found"));
		}
		
		getPlasmidDunetChart(plasmid);
		
		mav.addObject("plasmid", plasmid);
		mav.addObject("readOnly", true);
		
		return mav;
	}
	
	@RequestMapping(value = "search-form", method = RequestMethod.GET)
	public ModelAndView searchForm() {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_SEARCH.getPath());
		
		return mav;
	}
	
	@RequestMapping(value = "search", method = RequestMethod.POST)
	public ModelAndView search(@ModelAttribute("param") String param) {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_LIST.getPath());
		
		SearchResult<Plasmid> result = plasmidService.search(param);
		
		List<Plasmid> plasmidList = result.getResults();
		
		mav.addObject("plasmidList", plasmidList);
		
		return mav;
	}

	@Override
	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable Long id) {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_EDIT.getPath());
		
		Plasmid plasmid = plasmidService.find(id);
		
		getPlasmidDunetChart(plasmid);
		
		mav.addObject("plasmid", plasmid);
		mav.addObject("readOnly", false);
		
		return mav;
	}

	@Override
	@SecuredEnum(UserPermission.DATA_MANAGER)
	@RequestMapping(value = "remove/{id}", method = RequestMethod.GET)
	public ModelAndView remove(@PathVariable Long id) {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_LIST.getPath());
		
		Plasmid plasmid = plasmidService.find(id);
		
		if (plasmid != null) {
			plasmidService.remove(plasmid);
			
			mav = list();
			
			mav.addObject("msg", true);
			mav.addObject("message", MessageBundle.getMessageBundle("common.msg.remove.success"));
		} else {
			mav = list();
			
			mav.addObject("error", true);
			mav.addObject("message", MessageBundle.getMessageBundle("common.msg.remove.fail"));
		}
		
		return mav;
	}
	
	@RequestMapping(value = "input-data-assay/{id}", method = RequestMethod.GET)
	public ModelAndView inputDataAssay(@PathVariable Long id) {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_INPUT_DATA_ASSAY.getPath());
		
		Plasmid plasmid = plasmidService.find(id);
		mav.addObject("plasmid", plasmid);
		
		if (plasmid == null) {
			mav = list();
			
			mav.addObject("error", true);
			mav.addObject("message", MessageBundle.getMessageBundle("plasmid.msg.not.found"));
		} 
		
		return mav;
	}
	
	@RequestMapping(value = "input-data-assay", method = RequestMethod.POST)
	public ModelAndView inputDataAssay(@ModelAttribute("plasmidAssay") PlasmidAssayFormModel plasmidAssay) {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_INPUT_DATA_ASSAY.getPath());
		
		Plasmid plasmid = plasmidService.find(plasmidAssay.getPlasmidId());
		Enzyme initialEnzyme = enzymeService.find(plasmidAssay.getEnzymeInitialId());
		Enzyme finalEnzyme = enzymeService.find(plasmidAssay.getEnzymeFinalId());
		
		mav.addObject("plasmid", plasmid);
		mav.addObject("initialEnzyme", initialEnzyme);
		mav.addObject("finalEnzyme", finalEnzyme);
		mav.addObject("inputedName", plasmidAssay.getInputName());
		mav.addObject("inputedSequence", plasmidAssay.getInputSequence());
		
		mav.addObject("enzymeSequenceInitialCourtPosition", plasmidAssay.getEnzymeSequenceInitialCourtPosition());
		mav.addObject("enzymeSequenceFinalCourtPosition", plasmidAssay.getEnzymeSequenceFinalCourtPosition());
		
		mav.addObject("enzymePlasmidInitialCourtPositionList", plasmidAssay.getEnzymePlasmidInitialCourtPositionList());
		mav.addObject("enzymePlasmidFinalCourtPositionList", plasmidAssay.getEnzymePlasmidFinalCourtPositionList());
		
		if (plasmid == null) {
			mav = list();
			
			mav.addObject("error", true);
			mav.addObject("message", MessageBundle.getMessageBundle("plasmid.msg.not.found"));
		}
		
		return mav;
	}
	
	@RequestMapping(value = "show-elective-enzymes", method = RequestMethod.POST)
	public ModelAndView showElectiveEnzymes(@ModelAttribute("plasmidAssay") PlasmidAssayFormModel plasmidAssay) {
		ModelAndView mav = new ModelAndView(Navigation.ENZYME_ELECTIVE.getPath());
		
		List<Enzyme> enzymeList = enzymeService.processElectiveEnzymes(plasmidAssay.getInputSequence(), plasmidAssay.getPlasmidId());
		mav.addObject("enzymeList", enzymeList);
		
		Plasmid plasmid = plasmidService.find(plasmidAssay.getPlasmidId());
		mav.addObject("plasmid", plasmid);
		
		mav.addObject("inputedName", plasmidAssay.getInputName());
		mav.addObject("inputedSequence", plasmidAssay.getInputSequence());
		
		if (plasmid == null) {
			mav = list();
			
			mav.addObject("error", true);
			mav.addObject("message", MessageBundle.getMessageBundle("plasmid.msg.not.found"));
		}
		
		return mav;
	}
	
	@RequestMapping(value = "run-assay", method = RequestMethod.POST)
	public ModelAndView runAssaySimulation(@ModelAttribute("plasmidAssay") PlasmidAssayFormModel plasmidAssay) {
		ModelAndView mav = new ModelAndView(Navigation.PLASMID_VIEW.getPath());
		
		Plasmid plasmid = plasmidService.find(plasmidAssay.getPlasmidId());
		Plasmid newPlasmid = null;
		
		if (plasmid == null) {
			mav = list();
			
			mav.addObject("error", true);
			mav.addObject("message", MessageBundle.getMessageBundle("plasmid.msg.not.found"));
		} else {
			newPlasmid = plasmidService.runAssaySimulation(
					plasmid, 
					plasmidAssay.getInputName(),
					plasmidAssay.getInputSequence(), 
					plasmidAssay.getEnzymeInitialId(),
					plasmidAssay.getEnzymeFinalId(),
					plasmidAssay.getEnzymePlasmidInitialCourtPosition(),
					plasmidAssay.getEnzymePlasmidFinalCourtPosition(),
					plasmidAssay.getEnzymeSequenceInitialCourtPosition(), 
					plasmidAssay.getEnzymeSequenceFinalCourtPosition(),
					plasmidAssay.getDirection()
				);
			
			getPlasmidDunetChart(plasmid);
			
			if (newPlasmid != null) { 
				getPlasmidDunetChart(newPlasmid);
			}
		}
		
		mav.addObject("plasmid", plasmid);
		mav.addObject("newPlasmid", newPlasmid);
		mav.addObject("readOnly", true);
		
		return mav;
	}
	
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		dataBinder.setAutoGrowCollectionLimit(Integer.MAX_VALUE);
	}
}
