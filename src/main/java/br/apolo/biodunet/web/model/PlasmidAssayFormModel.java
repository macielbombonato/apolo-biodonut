package br.apolo.biodunet.web.model;

import java.util.List;

import br.apolo.biodunet.data.enums.DirectionEnum;


public class PlasmidAssayFormModel {
	
	private Long plasmidId;
	
	private String inputName;
	
	private String inputSequence;
	
	private Long enzymeInitialId;
	
	private Long enzymeFinalId;
	
	private Long enzymeSequenceInitialCourtPosition;
	
	private Long enzymeSequenceFinalCourtPosition;
	
	private Long enzymePlasmidInitialCourtPosition;
	
	private List<String> enzymePlasmidInitialCourtPositionList;
	
	private Long enzymePlasmidFinalCourtPosition;
	
	private List<String> enzymePlasmidFinalCourtPositionList;
	
	private DirectionEnum direction;
	
	public List<String> getEnzymePlasmidInitialCourtPositionList() {
		return enzymePlasmidInitialCourtPositionList;
	}

	public void setEnzymePlasmidInitialCourtPositionList(
			List<String> enzymePlasmidInitialCourtPositionList) {
		this.enzymePlasmidInitialCourtPositionList = enzymePlasmidInitialCourtPositionList;
	}

	public List<String> getEnzymePlasmidFinalCourtPositionList() {
		return enzymePlasmidFinalCourtPositionList;
	}

	public void setEnzymePlasmidFinalCourtPositionList(
			List<String> enzymePlasmidFinalCourtPositionList) {
		this.enzymePlasmidFinalCourtPositionList = enzymePlasmidFinalCourtPositionList;
	}

	public Long getPlasmidId() {
		return plasmidId;
	}

	public void setPlasmidId(Long plasmidId) {
		this.plasmidId = plasmidId;
	}

	public String getInputName() {
		return inputName;
	}

	public void setInputName(String inputName) {
		this.inputName = inputName;
	}

	public String getInputSequence() {
		return inputSequence;
	}

	public void setInputSequence(String inputSequence) {
		this.inputSequence = inputSequence;
	}

	public Long getEnzymeInitialId() {
		return enzymeInitialId;
	}

	public void setEnzymeInitialId(Long enzymeInitialId) {
		this.enzymeInitialId = enzymeInitialId;
	}

	public Long getEnzymeFinalId() {
		return enzymeFinalId;
	}

	public void setEnzymeFinalId(Long enzymeFinalId) {
		this.enzymeFinalId = enzymeFinalId;
	}

	public Long getEnzymeSequenceInitialCourtPosition() {
		return enzymeSequenceInitialCourtPosition;
	}

	public void setEnzymeSequenceInitialCourtPosition(
			Long enzymeSequenceInitialCourtPosition) {
		this.enzymeSequenceInitialCourtPosition = enzymeSequenceInitialCourtPosition;
	}

	public Long getEnzymeSequenceFinalCourtPosition() {
		return enzymeSequenceFinalCourtPosition;
	}

	public void setEnzymeSequenceFinalCourtPosition(
			Long enzymeSequenceFinalCourtPosition) {
		this.enzymeSequenceFinalCourtPosition = enzymeSequenceFinalCourtPosition;
	}

	public Long getEnzymePlasmidInitialCourtPosition() {
		return enzymePlasmidInitialCourtPosition;
	}

	public void setEnzymePlasmidInitialCourtPosition(
			Long enzymePlasmidInitialCourtPosition) {
		this.enzymePlasmidInitialCourtPosition = enzymePlasmidInitialCourtPosition;
	}

	public Long getEnzymePlasmidFinalCourtPosition() {
		return enzymePlasmidFinalCourtPosition;
	}

	public void setEnzymePlasmidFinalCourtPosition(
			Long enzymePlasmidFinalCourtPosition) {
		this.enzymePlasmidFinalCourtPosition = enzymePlasmidFinalCourtPosition;
	}

	public DirectionEnum getDirection() {
		return direction;
	}

	public void setDirection(DirectionEnum direction) {
		this.direction = direction;
	}

}
