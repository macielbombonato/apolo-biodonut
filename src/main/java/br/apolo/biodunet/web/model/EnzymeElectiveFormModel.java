package br.apolo.biodunet.web.model;

import br.apolo.biodunet.data.model.Enzyme;


public class EnzymeElectiveFormModel {
	
	private Long plasmidId;
	
	private String inputSequence;
	
	private Enzyme initialEnzyme;
	
	private Enzyme finalEnzyme;

	public Long getPlasmidId() {
		return plasmidId;
	}

	public void setPlasmidId(Long plasmidId) {
		this.plasmidId = plasmidId;
	}

	public String getInputSequence() {
		return inputSequence;
	}

	public void setInputSequence(String inputSequence) {
		this.inputSequence = inputSequence;
	}

	public Enzyme getInitialEnzyme() {
		return initialEnzyme;
	}

	public void setInitialEnzyme(Enzyme initialEnzyme) {
		this.initialEnzyme = initialEnzyme;
	}

	public Enzyme getFinalEnzyme() {
		return finalEnzyme;
	}

	public void setFinalEnzyme(Enzyme finalEnzyme) {
		this.finalEnzyme = finalEnzyme;
	}

}
