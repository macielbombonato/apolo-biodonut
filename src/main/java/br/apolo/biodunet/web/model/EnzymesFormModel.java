package br.apolo.biodunet.web.model;

import java.util.List;

import br.apolo.biodunet.data.model.Enzyme;

public class EnzymesFormModel {
	private List<Enzyme> enzymes;

	public List<Enzyme> getEnzymes() {
		return enzymes;
	}

	public void setEnzymes(List<Enzyme> enzymes) {
		this.enzymes = enzymes;
	}
}
