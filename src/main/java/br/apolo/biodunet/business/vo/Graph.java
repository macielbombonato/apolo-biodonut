package br.apolo.biodunet.business.vo;

import java.util.ArrayList;

/**
 *
 * @author will
 */
public class Graph {
    ArrayList<Node> nodes;
    
    public void addNode(Node n){
        nodes.add(n);
    }
    
    public void removeNode(Node n){
        nodes.remove(n);
    }
}
