package br.apolo.biodunet.business.vo;

import java.util.ArrayList;

import br.apolo.biodunet.data.model.Feature;

/**
 *
 * @author will
 */
public class Node {
    ArrayList<Node> edges;
    Feature feature;
    
    public Node(Feature f)
    {
        feature = f;
    }
    
    public void addEdge(Node n){
        edges.add(n);
    }
    
    public void removeEdge(Node n){
        edges.remove(n);
    }
}
