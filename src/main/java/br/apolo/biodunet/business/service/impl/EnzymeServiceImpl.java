package br.apolo.biodunet.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.apolo.biodunet.business.service.EnzymeService;
import br.apolo.biodunet.business.service.PlasmidService;
import br.apolo.biodunet.data.model.Enzyme;
import br.apolo.biodunet.data.model.Enzyme_;
import br.apolo.biodunet.data.model.Plasmid;
import br.apolo.biodunet.data.repository.EnzymeRepository;
import br.apolo.business.model.SearchResult;
import br.apolo.business.service.impl.BaseServiceImpl;

@Service("enzymeService")
public class EnzymeServiceImpl extends BaseServiceImpl<Enzyme> implements
		EnzymeService {

	@Autowired
	EnzymeRepository enzymeRepository;
	
	@Autowired
	PlasmidService plasmidService;

	@Override
	public List<Enzyme> list() {
		return (List<Enzyme>) enzymeRepository.findAllEnzymes();
	}

	@Override
	public Enzyme find(Long id) {
		return enzymeRepository.findOne(id);
	}

	@Override
	@Transactional
	public Enzyme save(Enzyme entity) {
		return enzymeRepository.save(entity);
	}

	@Override
	@Transactional
	public void remove(Enzyme entity) {
		enzymeRepository.delete(entity);
	}

	@Override
	public SearchResult<Enzyme> search(String param) {
		SearchResult<Enzyme> result = new SearchResult<Enzyme>();

		List<SingularAttribute<Enzyme, String>> fields = new ArrayList<SingularAttribute<Enzyme, String>>();
		fields.add(Enzyme_.name);
		fields.add(Enzyme_.accession);

		result.setResults(enzymeRepository.search(param, fields));

		return result;
	}

	public List<Enzyme> processElectiveEnzymes(String sequenceInserted, Long plasmidId) {
		List<Enzyme> electiveEnzymes = null;

		if (sequenceInserted != null && !"".equals(sequenceInserted)) {
			
			Plasmid plasmid = plasmidService.find(plasmidId);

			String plasmidSequenceStr = plasmid.getPlasmidSequence();

			electiveEnzymes = null;
			List<Enzyme> enzymes = enzymeRepository.findAllEnzymes();
			String enzymeSearchSequence = "";
			int elective = 0;
			boolean reverse = false;
			String complementCodon = "";
			String replaceValue = "";
			String reverseComplementCodon = "";
			String reverseReplaceValue = "";
			for (Enzyme enzyme : enzymes) {
				elective = 0;
				if (enzyme.getPrincipalValue() != null
						&& !"".equals(enzyme.getPrincipalValue())
						&& enzyme.getPrincipalValue().length() > 0) {

					reverse = false;
					complementCodon = "";
					replaceValue = "";

					if (enzyme.getComplementValue() != null
							&& !"".equals(enzyme.getComplementValue())
							&& enzyme.getComplementValue().length() > 0) {
						for (int i = 0; i < enzyme.getComplementValue()
								.length(); i++) {
							if (enzyme.getComplementValue().charAt(i) == 'A') {
								complementCodon += "T";
							} else if (enzyme.getComplementValue().charAt(i) == 'C') {
								complementCodon += "G";
							} else if (enzyme.getComplementValue().charAt(i) == 'T') {
								complementCodon += "A";
							} else if (enzyme.getComplementValue().charAt(i) == 'G') {
								complementCodon += "C";
							}

							if (enzyme.getPrincipalValue().contains(
									complementCodon)) {
								replaceValue = complementCodon;
							}
						}

						reverseComplementCodon = "";
						reverseReplaceValue = "";

						for (int i = enzyme.getComplementValue().length() - 1; i >= 0; i--) {
							if (enzyme.getComplementValue().charAt(i) == 'A') {
								reverseComplementCodon = "T"
										+ reverseComplementCodon;
							} else if (enzyme.getComplementValue().charAt(i) == 'C') {
								reverseComplementCodon = "G"
										+ reverseComplementCodon;
							} else if (enzyme.getComplementValue().charAt(i) == 'T') {
								reverseComplementCodon = "A"
										+ reverseComplementCodon;
							} else if (enzyme.getComplementValue().charAt(i) == 'G') {
								reverseComplementCodon = "C"
										+ reverseComplementCodon;
							}

							if (enzyme.getPrincipalValue().contains(
									reverseComplementCodon)) {
								reverseReplaceValue = reverseComplementCodon;
							}
						}

						if (reverseReplaceValue.length() > replaceValue
								.length()) {
							replaceValue = reverseReplaceValue;
							reverse = true;
						}

						if (complementCodon != null && replaceValue != null) {
							complementCodon = complementCodon.replace(
									replaceValue, "").trim();
						}
					}

					if (reverse) {
						enzymeSearchSequence = complementCodon
								+ enzyme.getPrincipalValue();

						if (complementCodon != null
								&& complementCodon.length() > 0
								&& enzyme != null) {
							Long courtSize = 0L;

							if (enzyme.getPrincipalCourtPosition() != null) {
								courtSize = enzyme.getPrincipalCourtPosition();
							}

							enzyme.setPrincipalCourtPosition(courtSize
									+ complementCodon.length());
						}
					} else {
						enzymeSearchSequence = enzyme.getPrincipalValue()
								+ complementCodon;
					}
				} else {
					enzymeSearchSequence = null;
				}

				elective += validateElective(sequenceInserted,
						enzymeSearchSequence, enzyme, false);
				elective += validateElective(plasmidSequenceStr,
						enzymeSearchSequence, enzyme, true);

				if (elective == 2) {
					if (electiveEnzymes == null) {
						electiveEnzymes = new ArrayList<Enzyme>();
					}
					electiveEnzymes.add(enzyme);
				}
			}
		}

		return electiveEnzymes;
	}

	private int validateElective(String validationString,
			String enzymeSearchSequence, Enzyme enzyme, boolean isPlasmid) {
		int elective = 0;
		if (validationString != null && enzymeSearchSequence != null
				&& validationString.contains(enzymeSearchSequence)) {

			int index = 0;
			for (int i = 0; i < validationString.length(); i++) {
				index = validationString.indexOf(enzymeSearchSequence, i);

				if (index == -1) {
					break;
				}

				if (enzyme.getPrincipalCourtPosition() != null) {
					index += enzyme.getPrincipalCourtPosition().intValue();
				}

				if (isPlasmid) {
					addPlasmidCourtPosition(enzyme, index);
				} else {
					addSequenceCourtPosition(enzyme, index);
				}

				i = index;
			}

			elective++;
		}

		return elective;
	}

	private void addSequenceCourtPosition(Enzyme enzyme, int index) {
		boolean addValue = true;

		if (enzyme.getSequenceCourtPosition() == null) {
			enzyme.setSequenceCourtPosition(new ArrayList<Long>());
		}

		if (enzyme.getSequenceCourtPosition().size() > 0) {
			if (enzyme.getSequenceCourtPosition().get(
					enzyme.getSequenceCourtPosition().size() - 1) != null
					&& enzyme.getSequenceCourtPosition()
							.get(enzyme.getSequenceCourtPosition().size() - 1)
							.equals(index)) {
				addValue = false;
			}
		}

		if (addValue) {
			enzyme.getSequenceCourtPosition().add(new Long(index));
		}
	}

	private void addPlasmidCourtPosition(Enzyme enzyme, int index) {
		boolean addValue = true;

		if (enzyme.getPlasmidCourtPosition() == null) {
			enzyme.setPlasmidCourtPosition(new ArrayList<String>());
		}

		if (enzyme.getPlasmidCourtPosition().size() > 0) {
			if (enzyme.getPlasmidCourtPosition().get(
					enzyme.getPlasmidCourtPosition().size() - 1) != null
					&& enzyme.getPlasmidCourtPosition()
							.get(enzyme.getPlasmidCourtPosition().size() - 1)
							.equals(index)) {
				addValue = false;
			}
		}

		if (addValue) {
			enzyme.getPlasmidCourtPosition().add(index + "");
		}
	}
}
