package br.apolo.biodunet.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.apolo.biodunet.business.service.PlasmidService;
import br.apolo.biodunet.data.enums.DirectionEnum;
import br.apolo.biodunet.data.model.Enzyme;
import br.apolo.biodunet.data.model.Feature;
import br.apolo.biodunet.data.model.Plasmid;
import br.apolo.biodunet.data.model.Plasmid_;
import br.apolo.biodunet.data.model.Reference;
import br.apolo.biodunet.data.model.Sequence;
import br.apolo.biodunet.data.repository.EnzymeRepository;
import br.apolo.biodunet.data.repository.PlasmidRepository;
import br.apolo.business.model.SearchResult;
import br.apolo.business.service.impl.BaseServiceImpl;

@Service("plasmidService")
public class PlasmidServiceImpl extends BaseServiceImpl<Plasmid> implements
		PlasmidService {

	@Autowired
	PlasmidRepository plasmidRepository;
	
	@Autowired
	EnzymeRepository enzymeRepository;

	@Override
	public List<Plasmid> list() {
		return (List<Plasmid>) plasmidRepository.findAllPlasmids();
	}

	@Override
	public Plasmid find(Long id) {
		return plasmidRepository.findOne(id);
	}

	@Override
	@Transactional
	public Plasmid save(Plasmid entity) {
		if (entity != null) {
			if (entity.getFeatures() != null && !entity.getFeatures().isEmpty()) {
				for (Feature feature : entity.getFeatures()) {
					feature.setPlasmid(entity);
				}
			}

			if (entity.getReferences() != null
					&& !entity.getReferences().isEmpty()) {
				for (Reference reference : entity.getReferences()) {
					reference.setPlasmid(entity);
				}
			}

			if (entity.getSequences() != null
					&& !entity.getSequences().isEmpty()) {
				for (Sequence sequence : entity.getSequences()) {
					sequence.setPlasmid(entity);
				}
			}
		}

		return plasmidRepository.save(entity);
	}

	@Override
	@Transactional
	public void remove(Plasmid entity) {
		plasmidRepository.delete(entity);
	}

	@Override
	public SearchResult<Plasmid> search(String param) {
		SearchResult<Plasmid> result = new SearchResult<Plasmid>();

		List<SingularAttribute<Plasmid, String>> fields = new ArrayList<SingularAttribute<Plasmid, String>>();
		fields.add(Plasmid_.accession);
		fields.add(Plasmid_.locus);
		fields.add(Plasmid_.definition);
		fields.add(Plasmid_.keywords);
		fields.add(Plasmid_.source);

		result.setResults(plasmidRepository.search(param, fields));

		return result;
	}

	public Plasmid runAssaySimulation(
			Plasmid plasmid, 
			String inputName,
			String inputSequence, 
			Long enzymeInitialId,
			Long enzymeFinalId,
			Long initialPlasmidCourt,
			Long finalPlasmidCourt,
			Long initialSequenceCourt, 
			Long finalSequenceCourt,
			DirectionEnum direction
		) {
		Plasmid newPlasmid = new Plasmid();

		if (plasmid != null) {
			try {
				Enzyme enzymeInitial = enzymeRepository.findOne(enzymeInitialId);
				Enzyme enzymeFinal = enzymeRepository.findOne(enzymeFinalId);
				
				newPlasmid.setAccession(plasmid.getAccession() + " + " + inputName);
				newPlasmid.setId(plasmid.getId());
				newPlasmid.setDefinition(plasmid.getDefinition());
				newPlasmid.setKeywords(plasmid.getKeywords());
				newPlasmid.setLocus(plasmid.getLocus());
				newPlasmid.setVersion(plasmid.getVersion());
				newPlasmid.setSource(plasmid.getSource());

				Long initialCourt = initialPlasmidCourt;
				Long finalCourt = finalPlasmidCourt;

				String sequenceCourted = inputSequence.substring(
						initialSequenceCourt.intValue(),
						finalSequenceCourt.intValue());

				String plasmidSequence = plasmid.getPlasmidSequence();

				if (plasmidSequence != null && plasmidSequence.length() > 0) {
					String newPlasmidSequenceStart = plasmidSequence.substring(
							0, initialCourt.intValue());
					String newPlasmidSequenceEnd = plasmidSequence
							.substring(finalCourt.intValue(),
									plasmidSequence.length() - 1);

					newPlasmid.setSequences(new ArrayList<Sequence>());

					newPlasmid.getSequences().addAll(
							generateSequences(newPlasmid,
									newPlasmidSequenceStart, sequenceCourted,
									newPlasmidSequenceEnd));
				}

				Long plasmidSize = plasmid.getSize();
				Long newPlasmidSize = newPlasmid.getSize();

				newPlasmid.setFeatures(new ArrayList<Feature>());

				if (plasmid.getFeatures() != null) {
					Feature f = null;
					for (Feature feature : plasmid.getFeatures()) {
						f = new Feature();

						f.setId(feature.getId());
						f.setName(feature.getName());
						f.setDetails(feature.getDetails());
						f.setDirection(feature.getDirection());
						f.setStartPosition(feature.getStartPosition()
								.longValue());
						f.setEndPosition(feature.getEndPosition().longValue());

						if (feature.getStartPosition() >= finalCourt
								&& feature.getEndPosition() >= finalCourt) {
							f.setStartPosition(feature.getStartPosition()
									.longValue()
									- (plasmidSize - newPlasmidSize));
							f.setEndPosition(feature.getEndPosition()
									.longValue()
									- (plasmidSize - newPlasmidSize));

							newPlasmid.getFeatures().add(f);
						} else if (feature.getStartPosition() <= initialCourt
								&& feature.getEndPosition() <= initialCourt) {
							newPlasmid.getFeatures().add(f);
						}
					}
				}

				Feature sequenceFeature = new Feature();
				sequenceFeature.setName(inputName);
				sequenceFeature.setPlasmid(newPlasmid);
				sequenceFeature.setStartPosition(initialCourt);
				sequenceFeature.setEndPosition(initialCourt
						+ sequenceCourted.length());
				sequenceFeature.setHighlight(true);
				sequenceFeature.setDirection(direction);

				newPlasmid.getFeatures().add(sequenceFeature);

				Feature initialEnzymeFeature = new Feature();
				initialEnzymeFeature.setName(enzymeInitial.getName());
				initialEnzymeFeature.setPlasmid(newPlasmid);
				initialEnzymeFeature.setStartPosition(initialCourt);
				initialEnzymeFeature.setEndPosition(initialCourt);
				initialEnzymeFeature.setHighlight(true);
				initialEnzymeFeature.setEnzymeFeature(true);
				initialEnzymeFeature.setDirection(DirectionEnum.NEUTRAL);

				newPlasmid.getFeatures().add(initialEnzymeFeature);

				Feature finalEnzymeFeature = new Feature();
				finalEnzymeFeature.setName(enzymeFinal.getName());
				finalEnzymeFeature.setPlasmid(newPlasmid);
				finalEnzymeFeature.setStartPosition(initialCourt
						+ sequenceCourted.length());
				finalEnzymeFeature.setEndPosition(initialCourt
						+ sequenceCourted.length());
				finalEnzymeFeature.setHighlight(true);
				finalEnzymeFeature.setEnzymeFeature(true);
				finalEnzymeFeature.setDirection(DirectionEnum.NEUTRAL);

				newPlasmid.getFeatures().add(finalEnzymeFeature);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return newPlasmid;
	}
	
	private List<Sequence> generateSequences(Plasmid newPlasmid, String newPlasmidSequenceStart, String sequenceCourted, String newPlasmidSequenceEnd) {
		List<Sequence> sequences = new ArrayList<Sequence>();
		
		int actualPosition = 0;
		
		actualPosition = preencherSequencia(newPlasmidSequenceStart, sequences, actualPosition, false);
		actualPosition = preencherSequencia(sequenceCourted, sequences, actualPosition, true);
		actualPosition = preencherSequencia(newPlasmidSequenceEnd, sequences, actualPosition, false);
		
		return sequences;
	}
	
	private int preencherSequencia(String strSequence, List<Sequence> sequences, int actualPosition, boolean isBold) {
		Sequence sequence;
		String fragment;
		int jumpSize = 10;
		int actualInitialPosition = 0;
		
		if (sequences != null 
				&& sequences.size() > 0 
				&& sequences.get(sequences.size() - 1) != null
				&& sequences.get(sequences.size() - 1).getValue() != null
				&& sequences.get(sequences.size() - 1).getValue().replaceAll("<b>", "").replaceAll("</b>", "").trim().length() < jumpSize) {
			fragment = strSequence.substring(0, jumpSize - sequences.get(sequences.size() - 1).getValue().replaceAll("<b>", "").replaceAll("</b>", "").trim().length());
			
			if (isBold) {
				sequences.get(sequences.size() - 1).setValue(sequences.get(sequences.size() - 1).getValue() + "<b>" + fragment.trim() + "</b>");
			} else {
				sequences.get(sequences.size() - 1).setValue(sequences.get(sequences.size() - 1).getValue() + fragment.trim());
			}
			
			strSequence = strSequence.substring(fragment.length(), strSequence.length() - 1);
			
			actualPosition += fragment.length();
			
			sequences.get(sequences.size() - 1).setSequenceInitialPosition(new Long(actualPosition));
		}
		
		while (strSequence.length() > actualInitialPosition) {
			sequence = new Sequence();
			if (strSequence.length() > (actualInitialPosition + jumpSize)) {
				fragment = strSequence.substring(actualInitialPosition, actualInitialPosition + jumpSize);
				
				actualPosition += jumpSize;
				sequence.setSequenceInitialPosition(new Long(actualPosition));
				actualInitialPosition += jumpSize;
				
			} else {
				fragment = strSequence.substring(actualInitialPosition, strSequence.length() - 1);
				
				sequence.setSequenceInitialPosition(new Long(fragment.length() + actualPosition));
				
				actualInitialPosition = sequence.getSequenceInitialPosition().intValue() + 1;
				actualPosition = sequence.getSequenceInitialPosition().intValue();
			}
			
			if (isBold) {
				sequence.setValue("<b>" + fragment.trim() + "</b>");
			} else {
				sequence.setValue(fragment.trim());
			}
			
			sequences.add(sequence);
		}
		return actualPosition;
	}
}
