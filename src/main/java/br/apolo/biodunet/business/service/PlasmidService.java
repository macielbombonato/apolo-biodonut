package br.apolo.biodunet.business.service;

import br.apolo.biodunet.data.enums.DirectionEnum;
import br.apolo.biodunet.data.model.Plasmid;
import br.apolo.business.service.BaseService;

public interface PlasmidService extends BaseService<Plasmid> {
	
	Plasmid runAssaySimulation(
			Plasmid plasmid, 
			String inputName,
			String inputSequence, 
			Long enzymeInitialId,
			Long enzymeFinalId,
			Long initialPlasmidCourt,
			Long finalPlasmidCourt,
			Long initialSequenceCourt, 
			Long finalSequenceCourt,
			DirectionEnum direction
		);

}