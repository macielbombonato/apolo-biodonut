package br.apolo.biodunet.business.service;

import java.util.List;

import br.apolo.biodunet.data.model.Enzyme;
import br.apolo.business.service.BaseService;

public interface EnzymeService extends BaseService<Enzyme> {

	List<Enzyme> processElectiveEnzymes(String sequenceInserted, Long plasmidId);
	
}